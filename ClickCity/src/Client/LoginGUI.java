package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import CustomGUI.CButton;
import CustomGUI.CLabel;

public class LoginGUI extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	private JTextField usernameTextField;
	private JPasswordField passwordTextField;
	private CLabel usernameLabel;
	private CLabel passwordLabel;
	private CLabel titleLabel;
	private CButton loginButton;
	private CButton playAsGuestButton;
	private JPanel mainPanel;
	private JPanel contents;
	private ClientManager cm;
	private BufferedImage image;
	
	public LoginGUI(ClientManager cm){
		super("Login");
		
		this.cm = cm;
		
		try {                
			image = ImageIO.read(new File("resources/images/LoginRegister/cityLogin.png"));
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}

		instantiateComponents();
		createGUI();
		addActionAdapters();
		setLocationRelativeTo(null);
	}
	
	private void instantiateComponents(){
		mainPanel = new JPanel();
		contents = new JPanel(){
			private static final long serialVersionUID = 1L;

			@Override
			  protected void paintComponent(Graphics g) {

			    super.paintComponent(g);
			        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
			}
		};
		usernameTextField = new JTextField(20);
		passwordTextField = new JPasswordField(20);
		
		titleLabel = new CLabel("Login...");
		usernameLabel = new CLabel("Username: ");
		passwordLabel = new CLabel("Password: ");
		
		loginButton = new CButton("Login");
		playAsGuestButton = new CButton("Play as Guest...");
	}
	
	private void createGUI(){
		setSize(640, 480);
		
		mainPanel.setLayout(new BorderLayout());
		add(mainPanel);
		
		mainPanel.add(contents);
		
		contents.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridy = 0;
		c.anchor = GridBagConstraints.CENTER;
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		contents.add(titleLabel, c);
		
		c.gridx = 0;
		c.gridy = 1; 
		contents.add(usernameLabel, c);
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		
		c.gridx = 1;
		contents.add(usernameTextField, c);
		
		c.gridx = 0;
		c.gridy = 2;
		contents.add(passwordLabel, c);
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		
		c.gridx = 1;
		contents.add(passwordTextField, c);
		
		c.gridx = 1;
		c.gridy = 4;
		
		loginButton.setPreferredSize(new Dimension(110, 20));
		contents.add(loginButton, c);
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		
		getRootPane().setDefaultButton(loginButton);
		
		c.gridx = 1;
		c.gridy = 5;
		c.insets = new Insets(3, 0, 3, 0);
		
		playAsGuestButton.setPreferredSize(new Dimension(110, 20));
		contents.add(playAsGuestButton, c);
		
		revalidate();
		repaint();
		
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	private void addActionAdapters(){
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//TODO, will send the user/password as an object to the server
				String user = usernameTextField.getText();
				char[] passtemp = passwordTextField.getPassword();
				String pass = new String(passtemp);
				String passEncrypt = encrypt(pass);
				cm.login(user, passEncrypt);
				loginButton.disableButton();
				playAsGuestButton.disableButton();
			}
		});
		
		playAsGuestButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				cm.playAsGuest();   
				dispose();
			}
		});
	}
	
	//Sam's encrypt function
	private String encrypt(String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return "";
        }
    }
	
	public void notifySuccess(boolean flag){
		loginButton.enableButton();
		playAsGuestButton.enableButton();
		if (flag){
			setEnabled(false);
			dispose();
		}
		else{
			JOptionPane.showMessageDialog(null, "Username or password incorrect!", "Login Failed", JOptionPane.ERROR_MESSAGE);
		}
	}
}
