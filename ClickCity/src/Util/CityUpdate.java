package Util;

public class CityUpdate extends Update {
	private static final long serialVersionUID = 1L;

	private boolean mIsCreating;
	private boolean mIsRemoving;
	private int x;
	private int y;
	private int worldX;
	private int worldY;
	private int mLevel;
	private String mOccupant;
	private boolean successful;
	
	public CityUpdate(boolean isCreating, boolean isRemoving, int x, int y, int worldX, int worldY, int level, String occupant){
		this.mIsCreating = isCreating;
		this.mIsRemoving = isRemoving;
		this.x = x;
		this.y = y;
		this.worldX = worldX;
		this.worldY = worldY;
		this.mLevel = level;
		this.mOccupant = occupant;
		successful = false;
	}

	public boolean isCreating() {
		return mIsCreating;
	}

	public boolean isRemoving() {
		return mIsRemoving;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getWorldX(){
		return worldX;
	}
	
	public int getWorldY(){
		return worldY;
	}

	public int getLevel() {
		return mLevel;
	}

	public String getOccupant() {
		return mOccupant;
	}
	
	public boolean successful(){
		return successful;
	}
	
	public void succeed(){
		successful = true;
	}
}
