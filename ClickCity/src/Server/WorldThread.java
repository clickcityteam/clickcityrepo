package Server;

import Util.WorldUpdate;

public class WorldThread extends Thread {
	private Server s;
	private int x;
	private int y;
	public WorldThread(Server S, int x, int y){
		this.s = S;
		this.x = x;
		this.y = y;
		this.start();
	}
	
	public void run(){
		try{
			while(true){
				synchronized(s.getMsql()){
					if(s.getMsql().updateWealth(x, y)){
						WorldUpdate wu = s.getMsql().getWorldUpdate(x, y);
						wu.succeed();
						s.getSM().PushToAllClients(wu);
					}
				}
				Thread.sleep(1000);
			}
		} catch (InterruptedException ie){
			ie.printStackTrace();
		}
	}
}
