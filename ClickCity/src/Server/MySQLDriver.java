package Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import com.mysql.jdbc.Driver;

import Util.CityUpdate;
import Util.InitUpdate;
import Util.Parser;
import Util.PlayerUpdate;
import Util.PropertyData;
import Util.ShopUpdate;
import Util.Upgrade;
import Util.Upgrade.UpgradeType;
import Util.WorldUpdate;
import Util.pair;

public class MySQLDriver {
	private Connection con;
	private final String DatabaseName = "ClickCity";
	private final String Username = Parser.getString("DatabaseUser", Parser.serverConfig);
	private final String Password = Parser.getString("DatabasePassword", Parser.serverConfig);
	private final String DatabaseAddress = "jdbc:mysql://localhost:3306/" + DatabaseName + "?user=" + Username + "&password=" + Password;
	
	private final String selectUsernamePassword = "SELECT HashedPassword FROM Username WHERE Username=?";
	private final String selectUsernameID = "SELECT usernameID FROM Username WHERE Username=?";
	private final String insertUsername = "INSERT INTO Username (Username, HashedPassword) VALUES(?,?)";
	private final String selectPlayer = "SELECT Apples, ApplesPerSecond, ApplesPerClick, CityBenefit FROM Player WHERE usernameID=?";
	private final String selectPlayerApples = "SELECT Apples FROM Player WHERE PlayerID=?";
	private final String selectPlayerID = "SELECT PlayerID FROM Player WHERE usernameID=?";
	private final String updatePlayerApples = "UPDATE Player SET Apples=? WHERE PlayerID=?";
	private final String initPlayer = "INSERT INTO Player (usernameID, Apples, ApplesPerSecond, ApplesPerClick, CityBenefit) VALUES(?,?,?,?,?)";
	private final String modifyPlayer = "UPDATE Player SET Apples=?, ApplesPerSecond=?, ApplesPerClick=?, CityBenefit=? WHERE usernameID=?";
	private final String selectApplesPerSecond = "SELECT ApplesPerSecond FROM Player WHERE PlayerID=?";
	private final String selectApplesPerClick = "SELECT ApplesPerClick FROM Player WHERE PlayerID=?";
	private final String selectPlayerCityBenefit = "SELECT CityBenefit FROM Player WHERE PlayerID=?";
	private final String upgradeApplesPerSecond = "UPDATE Player SET ApplesPerSecond=? WHERE PlayerID=?";
	private final String upgradeApplesPerClick = "UPDATE Player SET ApplesPerClick=? WHERE PlayerID=?";
	private final String upgradePlayerCityBenefit = "UPDATE Player SET CityBenefit=? WHERE PlayerID=?";
	private final String selectAllCities = "SELECT * FROM City";
	private final String makeCity = "INSERT INTO City (X, Y, Tier, LandValue, PlayerOwnerBenefit, PlayerOwnerID) VALUES(?,?,?,?,?,?)";
	private final String updateCity = "UPDATE City SET Tier=?, LandValue=?, PlayerOwnerBenefit=? WHERE X=? AND Y=?";
	private final String updateTierOfProperty = "UPDATE Property SET Tier=? WHERE PlayerOccupantID=?";
	private final String insertProperty = "INSERT INTO Property (CityID, X, Y, PlayerOccupantID, Tier) VALUES(?,?,?,?,?)";
	private final String removeProperty = "DELETE FROM Property WHERE PlayerOccupantID=?";
	private final String selectAllUpgrades = "SELECT * FROM UpgradeType";
	private final String selectUpgradeID = "SELECT UpgradeID FROM UpgradeType WHERE UpgradeName=?";
	private final String selectAllUpgradesBought = "SELECT UpgradeID, Amount FROM UpgradesBought WHERE PlayerOwnerID=?";
	private final String selectUpgradesBought = "SELECT Amount FROM UpgradesBought WHERE UpgradeID=? AND PlayerOwnerID=?";
	private final String insertNewUpgradeBought = "INSERT INTO UpgradesBought (UpgradeID, Amount, PlayerOwnerID) VALUES(?,?,?)";
	private final String updateUpgradesBought = "UPDATE UpgradesBought SET Amount=? WHERE UpgradeID=? AND PlayerOwnerID=?";
	
	private final String getAllPlayersInCity = "SELECT p.Apples, p.ApplesPerSecond, p.ApplesPerClick, p.CityBenefit FROM Player p, Property prop, City c WHERE p.PlayerID=prop.PlayerOccupantID AND prop.CityID = c.CityID AND c.X=? AND c.Y=?";
	private final String getLandValueOfPlayer = "SELECT c.LandValue FROM City c, Property prop WHERE prop.CityID = c.CityID AND prop.PlayerOccupantID=?";
	private final String getOwnerBenefits = "SELECT PlayerOwnerBenefit FROM City WHERE PlayerOwnerID=?";
	private final String getCityID = "SELECT CityID FROM City WHERE X=? AND Y=?";
	private final String getCurrentProperty = "SELECT CityID, X, Y, Tier, PlayerOccupantID FROM Property WHERE PlayerOccupantID=?";
	private final String getCityXY = "SELECT X, Y FROM City WHERE CityID=?";
	private final String getUpgradesBoughtByPlayer = "SELECT ut.BaseCost, ut.MultiplicativeCost, ub.Amount FROM UpgradeType ut, UpgradesBought ub WHERE PlayerOwnerID=? AND UpgradeName=? AND ut.UpgradeID=ub.UpgradeID";
	private final String selectUpgrade = "SELECT BaseCost, MultiplicativeCost FROM UpgradeType WHERE UpgradeName=?";
	private final String getUpgradeEffectAndType = "SELECT UpgradeType, Effect FROM UpgradeType WHERE UpgradeName=?";
	private final String getAllCities = "SELECT c.X, c.Y, u.Username, c.Tier FROM City c, Player p, Username u WHERE c.PlayerOwnerID=p.PlayerID AND p.usernameID=u.usernameID";
	private final String getAllProperties = "SELECT prop.X, prop.Y, c.X, c.Y, prop.Tier, u.Username FROM Property prop, City c, Player p, Username u WHERE prop.PlayerOccupantID=p.PlayerID AND p.usernameID=u.usernameID AND prop.CityID=c.CityID";
	private final String selectCity = "SELECT c.X, c.Y, c.Tier, u.Username FROM City c, Player p, Username u WHERE c.PlayerOwnerID = p.PlayerID AND p.usernameID = u.usernameID AND c.X=? AND c.Y=?";
	private final String selectPropertyTierStuff = "SELECT prop.X, prop.Y, c.X, c.Y, prop.Tier, p.ApplesPerSecond, p.ApplesPerClick FROM City c, Property prop, Player p, Username u WHERE u.Username=? AND p.usernameID = u.usernameID AND p.PlayerID = prop.PlayerOccupantID AND c.CityID = prop.CityID";
	
	public MySQLDriver(){
		try{
			new Driver();
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	public void connect(){
		try{
			con = DriverManager.getConnection(DatabaseAddress);
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void stop(){
		try{
			if(con != null){
				con.close();
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	public boolean userExists(String username){
		try {
			connect();
			PreparedStatement ps = con.prepareStatement(selectUsernameID);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				stop();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	public boolean authenticate(String username, String hashedPassword){
		try {
			connect();
			PreparedStatement ps = con.prepareStatement(selectUsernamePassword);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				if(rs.getString(1).equals(hashedPassword)){
					stop();
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	public boolean addUserPassword(String username, String hashedPassword){
		try {
			connect();
			PreparedStatement ps = con.prepareStatement(insertUsername);
			ps.setString(1, username);
			ps.setString(2, hashedPassword);
			ps.executeUpdate();
			stop();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	private float getCityBenefit(int playerID){
		try{
			PreparedStatement ps = con.prepareStatement(getLandValueOfPlayer);
			ps.setInt(1, playerID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				return rs.getFloat(1);
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return 0.0f;
	}
	
	private float getOwnerBenefit(int playerID){
		try{
			PreparedStatement ps = con.prepareStatement(getOwnerBenefits);
			ps.setInt(1, playerID);
			ResultSet rs = ps.executeQuery();
			float benefit = 0.0f;
			while(rs.next()){
				benefit += rs.getFloat(1);
			}
			return benefit;
		} catch (SQLException e){
			e.printStackTrace();
		}
		return 0.0f;
	}
	
	public boolean UpdatePlayer(String username){
		try {
			connect();
			int playerID = getPlayerID(username);
			if(playerID != -1){
				PreparedStatement psPlayer = con.prepareStatement(selectPlayer);
				psPlayer.setInt(1, playerID);
				ResultSet rsPlayer = psPlayer.executeQuery();
				int currApples = -1;
				float currApplesPerSecond = -0.0f;
				float currApplesPerClick = -0.0f;
				float currCityBenefit = -0.0f;
				while(rsPlayer.next()){
					currApples = rsPlayer.getInt(1);
					currApplesPerSecond = rsPlayer.getFloat(2);
					currApplesPerClick = rsPlayer.getFloat(3);
					currCityBenefit = rsPlayer.getFloat(4);
					break;
				}
				
				float cityBenefit = getCityBenefit(playerID);
				connect();
				float ownerBenefit = getOwnerBenefit(playerID);
				
				int newApples = currApples + (int)(currApplesPerSecond + cityBenefit + ownerBenefit);
				if(newApples > 1000000000){
					newApples = 1000000000;
				}
				PreparedStatement psUpdate = con.prepareStatement(modifyPlayer);
				psUpdate.setInt(1, newApples);
				psUpdate.setFloat(2, currApplesPerSecond);
				psUpdate.setFloat(3, currApplesPerClick);
				psUpdate.setFloat(4, currCityBenefit);
				psUpdate.setInt(5, playerID);
				psUpdate.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	public boolean CreatePlayer(String username, int apples, float applesPerSecond, float applesPerClick, float cityBenefit){
		try {
			connect();
			PreparedStatement ps = con.prepareStatement(selectUsernameID);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			int usernameID = -1;
			while(rs.next()){
				usernameID = rs.getInt(1);
			}
			if(usernameID != -1){
				PreparedStatement psInit = con.prepareStatement(initPlayer);
				psInit.setInt(1, usernameID);
				psInit.setInt(2, apples);
				psInit.setFloat(3, applesPerSecond);
				psInit.setFloat(4, applesPerClick);
				psInit.setFloat(5, cityBenefit);
				psInit.executeUpdate();
				stop();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	public boolean makeWorld(WorldUpdate wu){
		try {
			connect();
			String owner = wu.getOwner();
			
			if(!hasEnoughApples(owner, 100000)){
				return false;
			}
			
			int playerID = getPlayerID(owner);
			
			int x = wu.getX();
			int y = wu.getY();
			int Tier = wu.getTier();
			PreparedStatement psMakeCity = con.prepareStatement(makeCity);
			psMakeCity.setInt(1, x);
			psMakeCity.setInt(2, y);
			psMakeCity.setInt(3, Tier);
			psMakeCity.setFloat(4, 0.0f);
			psMakeCity.setFloat(5, 0.0f);
			psMakeCity.setInt(6, playerID);
			psMakeCity.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	public WorldUpdate getWorldUpdate(int x, int y){
		try{
			connect();
			PreparedStatement ps = con.prepareStatement(selectCity);
			ps.setInt(1, x);
			ps.setInt(2, y);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				WorldUpdate wu = new WorldUpdate(rs.getInt(1), rs.getInt(2), rs.getString(4), rs.getInt(3));
				return wu;
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally{
			stop();
		}
		return null;
	}
	
	public PlayerUpdate getPlayerUpdate(String username){
		try{
			connect();
			int playerID = getPlayerID(username);
			
			if(playerID != -1){
				PreparedStatement psPlayer = con.prepareStatement(selectPlayer);
				psPlayer.setInt(1, playerID);
				ResultSet rsPlayer = psPlayer.executeQuery();
				int currApples = -1;
				float currApplesPerSecond = -0.0f;
				float currApplesPerClick = -0.0f;
				float currCityBenefit = -0.0f;
				while(rsPlayer.next()){
					currApples = rsPlayer.getInt(1);
					currApplesPerSecond = rsPlayer.getFloat(2);
					currApplesPerClick = rsPlayer.getFloat(3);
					currCityBenefit = rsPlayer.getFloat(4);
					break;
				}
				PlayerUpdate pu = new PlayerUpdate(currApples, currApplesPerSecond, currApplesPerClick, currCityBenefit);
				return pu;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
	public boolean click(String username, String propertyOwner) {
		try {
			connect();
			int playerID = getPlayerID(username);
			int propertyOwnerID = getPlayerID(propertyOwner);
			if(playerID != -1 && propertyOwnerID != -1){
				PreparedStatement psPlayer = con.prepareStatement(selectPlayer);
				psPlayer.setInt(1, playerID);
				ResultSet rsPlayer = psPlayer.executeQuery();
				int currApples = -1;
				float currApplesPerSecond = -0.0f;
				float currApplesPerClick = -0.0f;
				float currCityBenefit = -0.0f;
				while(rsPlayer.next()){
					currApples = rsPlayer.getInt(1);
					currApplesPerSecond = rsPlayer.getFloat(2);
					currApplesPerClick = rsPlayer.getFloat(3);
					currCityBenefit = rsPlayer.getFloat(4);
					break;
				}
				
				PreparedStatement psPropertyOwner = con.prepareStatement(selectPlayer);
				psPropertyOwner.setInt(1, propertyOwnerID);
				ResultSet rsPropertyOwner = psPropertyOwner.executeQuery();
				float currPropertyOwnerApplesPerClick = -0.0f;
				while(rsPropertyOwner.next()){
					currPropertyOwnerApplesPerClick = rsPropertyOwner.getFloat(3);
					break;
				}
				
				
				int newApples = currApples + (int)currPropertyOwnerApplesPerClick;
				if(newApples > 1000000000){
					newApples = 1000000000;
				}
				PreparedStatement psUpdate = con.prepareStatement(modifyPlayer);
				psUpdate.setInt(1, newApples);
				psUpdate.setFloat(2, currApplesPerSecond);
				psUpdate.setFloat(3, currApplesPerClick);
				psUpdate.setFloat(4, currCityBenefit);
				psUpdate.setInt(5, playerID);
				psUpdate.executeUpdate();
				stop();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	
	private int getCityTier(float cityWealth){
		int tier = 1;
		float[] tiers = {250.0f, 1000.0f};// add city tiers, at least 2
		for(int i = 0; i < tiers.length; i++){
			if(cityWealth > tiers[i]){
				tier = i + 2;
			}
		}
		return tier;
	}
	
	public boolean updateWealth(int x, int y){
		try{
			connect();
			PreparedStatement ps = con.prepareStatement(getAllPlayersInCity);
			ps.setInt(1, x);
			ps.setInt(2, y);
			ResultSet rs = ps.executeQuery();
			float ownerWealth = 0.0f;
			float cityWealth = 0.0f;
			while(rs.next()){
				ownerWealth += rs.getFloat(2);
				cityWealth += rs.getFloat(4);
			}
			int prevTier = 1;
			PreparedStatement psTier = con.prepareStatement(selectCity);
			psTier.setInt(1, x);
			psTier.setInt(2, y);
			ResultSet rsTier = psTier.executeQuery();
			while(rsTier.next()){
				prevTier = rsTier.getInt(3);
				break;
			}
			int Tier = getCityTier(cityWealth);
			ownerWealth *= 0.1f;			
			
			PreparedStatement psCity = con.prepareStatement(updateCity);
			psCity.setInt(1, Tier);
			psCity.setFloat(2, cityWealth);
			psCity.setFloat(3, ownerWealth);
			psCity.setInt(4, x);
			psCity.setInt(5, y);
			psCity.executeUpdate();
			return Tier != prevTier;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	private boolean canAffordUpgrade(String username, String upgradeName){
		try{
			int playerID = getPlayerID(username);
			PreparedStatement psUpgrade = con.prepareStatement(getUpgradesBoughtByPlayer);
			psUpgrade.setInt(1, playerID);
			psUpgrade.setString(2, upgradeName);
			ResultSet rsUpgrade = psUpgrade.executeQuery();
			int amount = 0;
			if(rsUpgrade.next()){
				amount = rsUpgrade.getInt(3);
			}
			
			PreparedStatement psUpgradeType = con.prepareStatement(selectUpgrade);
			psUpgradeType.setString(1, upgradeName);
			ResultSet rsUpgradeType = psUpgradeType.executeQuery();
			int base = 0;
			float multiplier = 0.0f;
			while(rsUpgradeType.next()){
				base = rsUpgradeType.getInt(1);
				multiplier = rsUpgradeType.getFloat(2);
			}
			int cost = base * (int)Math.pow((double)multiplier, amount);
			return hasEnoughApples(username, cost);
		} catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean addUpgrade(String user, String upgradeName) {
		try{
			connect();
			if(!canAffordUpgrade(user, upgradeName)){
				return false;
			}
			PreparedStatement ps = con.prepareStatement(selectUpgradeID);
			ps.setString(1, upgradeName);
			ResultSet rs = ps.executeQuery();
			int upgradeID = -1;
			while(rs.next()){
				upgradeID = rs.getInt(1);
				break;
			}
			
			int playerID = getPlayerID(user);
			
			if(upgradeID != -1 && playerID != -1){
				PreparedStatement psUpgrade = con.prepareStatement(selectAllUpgradesBought);
				psUpgrade.setInt(1, playerID);
				ResultSet rsUpgrade = psUpgrade.executeQuery();
				boolean neverBought = false;
				while(rsUpgrade.next()){
					if(upgradeID == rsUpgrade.getInt(1)){
						PreparedStatement psUpdate = con.prepareStatement(updateUpgradesBought);
						psUpdate.setInt(1, rsUpgrade.getInt(2) + 1);
						psUpdate.setInt(2, upgradeID);
						psUpdate.setInt(3, playerID);
						psUpdate.executeUpdate();
						neverBought = true;
						break;
					}
				}
				
				if(!neverBought){
					PreparedStatement psInsert = con.prepareStatement(insertNewUpgradeBought);
					psInsert.setInt(1, upgradeID);
					psInsert.setInt(2, 1);
					psInsert.setInt(3, playerID);
					psInsert.executeUpdate();
				}
				
				PreparedStatement psNewUpgrade = con.prepareStatement(getUpgradeEffectAndType);
				psNewUpgrade.setString(1, upgradeName);
				ResultSet rsNewUpgrade = psNewUpgrade.executeQuery();
				if(rsNewUpgrade.next()){
					String type = rsNewUpgrade.getString(1);
					float effect = rsNewUpgrade.getFloat(2);
					String select = "";
					String upgrade = "";
					if(type.equals("rate")){
						select = selectApplesPerSecond;
						upgrade = upgradeApplesPerSecond;
					}
					else if(type.equals("click")){
						select = selectApplesPerClick;
						upgrade = upgradeApplesPerClick;
					}
					else if(type.equals("city")){
						select = selectPlayerCityBenefit;
						upgrade = upgradePlayerCityBenefit;
					}
					
					PreparedStatement psSelect = con.prepareStatement(select);
					psSelect.setInt(1, playerID);
					ResultSet rsSelect = psSelect.executeQuery();
					float num = 0.0f;
					while(rsSelect.next()){
						num = rsSelect.getFloat(1);
						break;
					}
					PreparedStatement psUpdate = con.prepareStatement(upgrade);
					psUpdate.setFloat(1, num + effect);
					psUpdate.setInt(2, playerID);
					psUpdate.executeUpdate();
					
					return true;
				}
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return false;
	}
	public ShopUpdate getShopUpdate(String user) {
		try{
			connect();
			int playerID = getPlayerID(user);
			if(playerID != -1){
				Vector<Upgrade> upgrades = new Vector<Upgrade>();
				PreparedStatement psUpgrades = con.prepareStatement(selectAllUpgrades);
				ResultSet rsUpgrades = psUpgrades.executeQuery();
				while(rsUpgrades.next()){
					PreparedStatement psUpgradesBought = con.prepareStatement(selectUpgradesBought);
					psUpgradesBought.setInt(1, rsUpgrades.getInt(1));
					psUpgradesBought.setInt(2, playerID);
					ResultSet rsUpgradesBought = psUpgradesBought.executeQuery();
					int amount;
					if(rsUpgradesBought.next()){
						amount = rsUpgradesBought.getInt(1);
					}
					else{
						amount = 0;
					}
					String upgradeType = rsUpgrades.getString(3);
					UpgradeType ut = null;
					if(upgradeType.equals("rate")){
						ut = UpgradeType.passiveGenerator;
					}
					else if(upgradeType.equals("click")){
						ut = UpgradeType.clicker;
					}
					else if(upgradeType.equals("city")){
						ut = UpgradeType.city;
					}
					Upgrade u = new Upgrade(ut, rsUpgrades.getString(2), rsUpgrades.getInt(4), rsUpgrades.getFloat(5), rsUpgrades.getFloat(6), amount);
					upgrades.add(u);
				}
				return new ShopUpdate(upgrades);
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
	
	private int getPlayerID(String username){
		try{
			PreparedStatement psUser = con.prepareStatement(selectUsernameID);
			psUser.setString(1, username);
			ResultSet rsUser = psUser.executeQuery();
			int userID = -1;
			while(rsUser.next()){
				userID = rsUser.getInt(1);
				break;
			}
			
			PreparedStatement psPlayer = con.prepareStatement(selectPlayerID);
			psPlayer.setInt(1, userID);
			ResultSet rsPlayer = psPlayer.executeQuery();
			int playerID = -1;
			while(rsPlayer.next()){
				playerID = rsPlayer.getInt(1);
				return playerID;
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return -1;
	}
	public PropertyData moveToProperty(String occupant, int worldX, int worldY, int x, int y) {
		try{
			connect();
			CityUpdate created = null;
			CityUpdate destroyed = null;
			int playerID = getPlayerID(occupant);
			int cost = 50;
			PreparedStatement psGetProperty = con.prepareStatement(getCurrentProperty);
			psGetProperty.setInt(1, playerID);
			ResultSet rsGetProperty = psGetProperty.executeQuery();
			while(rsGetProperty.next()){
				cost = 25000;
				break;
			}
			if(!hasEnoughApples(occupant, cost)){
				return null;
			}
			
			
			PreparedStatement psGetCurrProperty = con.prepareStatement(getCurrentProperty);
			psGetCurrProperty.setInt(1, playerID);
			ResultSet rsGetCurrProperty = psGetCurrProperty.executeQuery();
			int level = 1;
			while(rsGetCurrProperty.next()){
				PreparedStatement psGetCityXY = con.prepareStatement(getCityXY);
				psGetCityXY.setInt(1, rsGetCurrProperty.getInt(1));
				ResultSet rsGetCityXY = psGetCityXY.executeQuery();
				int currWorldX = -1;
				int currWorldY = -1;
				while(rsGetCityXY.next()){
					currWorldX = rsGetCityXY.getInt(1);
					currWorldY = rsGetCityXY.getInt(2);
					break;
				}
				
				PreparedStatement psDestroyCity = con.prepareStatement(removeProperty);
				psDestroyCity.setInt(1, playerID);
				psDestroyCity.executeUpdate();
				level = rsGetCurrProperty.getInt(4);
				destroyed = new CityUpdate(false, true, rsGetCurrProperty.getInt(2), rsGetCurrProperty.getInt(3), currWorldX, currWorldY, rsGetCurrProperty.getInt(4), occupant);
			}

			PreparedStatement psGetCityID = con.prepareStatement(getCityID);
			psGetCityID.setInt(1, worldX);
			psGetCityID.setInt(2, worldY);
			ResultSet rsGetCityID = psGetCityID.executeQuery();
			int worldID = -1;
			while(rsGetCityID.next()){
				worldID = rsGetCityID.getInt(1);
			}

			PreparedStatement psMakeProperty = con.prepareStatement(insertProperty);
			psMakeProperty.setInt(1, worldID);
			psMakeProperty.setInt(2, x);
			psMakeProperty.setInt(3, y);
			psMakeProperty.setInt(4, playerID);
			psMakeProperty.setInt(5, level);
			psMakeProperty.executeUpdate();
			created = new CityUpdate(true, false, x, y, worldX, worldY, level, occupant);
			return new PropertyData(true, created, destroyed);
			
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
	
	public CityUpdate updatePropertyTier(String username){
		try{
			connect();
			PreparedStatement ps = con.prepareStatement(selectPropertyTierStuff);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			CityUpdate cu = null;
			while(rs.next()){
				int currTier = rs.getInt(5);
				int newTier = getNewPropertyTier(rs.getFloat(6) + rs.getFloat(7));
				if(currTier != newTier){
					PreparedStatement psUpdateProperty = con.prepareStatement(updateTierOfProperty);
					psUpdateProperty.setInt(1, newTier);
					psUpdateProperty.setInt(2, getPlayerID(username));
					psUpdateProperty.executeUpdate();
					cu = new CityUpdate(false, false, rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), newTier, username);
				}
				break;
			}
			return cu;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
	
	private int getNewPropertyTier(float propertyWealth) {
		int tier = 1;
		float[] tiers = {1000.0f, 10000.0f}; // add property tiers, at least 2
		for(int i = 0; i < tiers.length; i++){
			if(propertyWealth > tiers[i]){
				tier = i + 2;
			}
		}
		return tier;
	}
	private boolean hasEnoughApples(String username, int amount){
		try{
			int playerID = getPlayerID(username);
			PreparedStatement ps = con.prepareStatement(selectPlayerApples);
			ps.setInt(1, playerID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				if(amount > rs.getInt(1)){
					return false;
				}
				else{
					int newApples = rs.getInt(1) - amount;
					PreparedStatement psUpdate = con.prepareStatement(updatePlayerApples);
					psUpdate.setInt(1, newApples);
					psUpdate.setInt(2, playerID);
					psUpdate.executeUpdate();
					return true;
				}
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	public Vector<pair> getAllWorldTiles() {
		Vector<pair> worldTiles = new Vector<pair>();
		try{
			connect();
			PreparedStatement ps = con.prepareStatement(selectAllCities);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				pair newPair = new pair();
				newPair.x = rs.getInt(2);
				newPair.y = rs.getInt(3);
				worldTiles.add(newPair);
			}
			return worldTiles;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
	public InitUpdate fillInit(InitUpdate iu) {
		
		String username = iu.getUsername();
		PlayerUpdate pu = getPlayerUpdate(username);
		ShopUpdate su = getShopUpdate(username);
		ArrayList<WorldUpdate> wus = getAllWorldUpdates();
		ArrayList<CityUpdate> cus = getAllCityUpdates();
		InitUpdate output = new InitUpdate(wus, cus, pu, su, username);
		if(pu == null || su == null || wus == null || cus == null){
			return null;
		}
		return output;
		
	}
	private ArrayList<CityUpdate> getAllCityUpdates() {
		try{
			connect();
			PreparedStatement ps = con.prepareStatement(getAllProperties);
			ResultSet rs = ps.executeQuery();
			ArrayList<CityUpdate> output = new ArrayList<CityUpdate>();
			while(rs.next()){
				CityUpdate cu = new CityUpdate(true, false, rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getString(6));
				cu.succeed();
				output.add(cu);
			}
			return output;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
	private ArrayList<WorldUpdate> getAllWorldUpdates() {
		try{
			connect();
			PreparedStatement ps = con.prepareStatement(getAllCities);
			ResultSet rs = ps.executeQuery();
			ArrayList<WorldUpdate> output = new ArrayList<WorldUpdate>();
			while(rs.next()){
				WorldUpdate wu = new WorldUpdate(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4));
				wu.succeed();
				output.add(wu);
			}
			return output;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			stop();
		}
		return null;
	}
}
