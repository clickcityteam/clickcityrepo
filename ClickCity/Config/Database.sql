DROP DATABASE if exists ClickCity;

CREATE DATABASE ClickCity;

USE ClickCity;

CREATE TABLE Username (
	usernameID int(11) primary key not null auto_increment,
	Username varchar(50) not null,
	HashedPassword varchar(50) not null
);

CREATE TABLE Player (
	PlayerID int(11) primary key not null auto_increment,
	usernameID int(11) not null,
	Apples int(50) not null,
	ApplesPerSecond float(50) not null,
	ApplesPerClick float(50) not null,
    CityBenefit float(50) not null,
	FOREIGN KEY fk1(usernameID) REFERENCES Username(usernameID)
);

CREATE TABLE City (
	CityID int(11) primary key not null auto_increment,
	X int(11) not null,
	Y int(11) not null,
    Tier int(2) not null,
	LandValue float(50) not null,
    PlayerOwnerBenefit float(50) not null,
	PlayerOwnerID int(11) not null,
	FOREIGN KEY fk1(PlayerOwnerID) REFERENCES Player(PlayerID)
);

CREATE TABLE Property (
	PropertyID int(11) primary key not null auto_increment,
    CityID int(11) not null,
    X int(11) not null,
    Y int(11) not null,
    Tier int(2) not null,
    PlayerOccupantID int(11) not null,
    FOREIGN KEY fk1(CityID) REFERENCES City(CityID),
    FOREIGN KEY fk2(PlayerOccupantID) REFERENCES Player(PlayerID)
);

CREATE TABLE UpgradeType (
	UpgradeID int(11) primary key not null auto_increment,
    UpgradeName varchar(50) not null,
    UpgradeType varchar(50) not null,
    BaseCost int(50) not null,
    MultiplicativeCost float(50) not null,
    Effect float(50) not null
);

CREATE Table UpgradesBought (
	UpgradeID int(11) not null,
    Amount int(11) not null,
    PlayerOwnerID int(11) not null,
    FOREIGN KEY fk1(UpgradeID) REFERENCES UpgradeType(UpgradeID),
    FOREIGN KEY fk2(PlayerOwnerID) REFERENCES Player(PlayerID)
);

INSERT INTO Username (Username, HashedPassword) VALUES ('guest', 'abcdef');
INSERT INTO Username (Username, HashedPassword) VALUES ('god', 'iamawesome');
INSERT INTO Player (usernameID, Apples, ApplesPerSecond, ApplesPerClick, CityBenefit) VALUES (1, 0, 0, 0, 0);
INSERT INTO Player (usernameID, Apples, ApplesPerSecond, ApplesPerClick, CityBenefit) VALUES (2, 0, 0, 1, 0);
INSERT INTO City (X, Y, Tier, LandValue, PlayerOwnerBenefit, PlayerOwnerID) VALUES (0, 0, 1, 0.0, 0.0, 2);
INSERT INTO Property (CityID, X, Y, Tier, PlayerOccupantID) VALUES (1, 0, 0, 1, 2);

INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Basket', 'click', 10, 1.25, 0.5);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Stool', 'click', 100, 1.25, 5.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Ladder', 'click', 1000, 1.2, 50.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Axe', 'click', 10000, 1.2, 100.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Barrel', 'click', 100000, 1.1, 500.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Johnny Appleseed Gloves', 'click', 1000000, 1.1, 1000.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Battering Ram', 'click', 10000000, 1.05, 5000.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Earthshaker Stick', 'click', 100000000, 1.05, 10000.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Magic Wand', 'click', 1000000000, 1.03, 30000.0);

INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Apple Tree', 'rate', 50, 2, 1.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Apple Orchard', 'rate', 300, 1.2, 5.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Apple-Picking Employee Tree', 'rate', 1000, 1.25, 30.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Apple Factory', 'rate', 10000, 1.25, 75.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Apple Cloning Machine', 'rate', 80000, 1.25, 300.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('GMO SuperApples', 'rate', 1000000, 1.5, 3000.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Apple Manufacturing Miniverse', 'rate', 3000000, 1.25, 5000.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Sentient Apple', 'rate', 10000000, 1.5, 10000.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Eden Machine', 'rate', 100000000, 3.0, 100000.0);

INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Town Well', 'city', 5000, 1.5, 10.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Town Square', 'city', 20000, 1.25, 30.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Skyscraper', 'city', 50000, 1.25, 50.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Coliseum', 'city', 200000, 1.25, 100.0);
INSERT INTO UpgradeType (UpgradeName, UpgradeType, BaseCost, MultiplicativeCost, Effect) VALUES ('Whole Foods', 'city', 2000000, 1.5, 500.0);
