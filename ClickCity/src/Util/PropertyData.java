package Util;

public class PropertyData {
	public PropertyData(boolean b, CityUpdate created, CityUpdate destroyed) {
		this.successful = b;
		this.created = created;
		this.destroyed = destroyed;
	}
	public boolean successful;
	public CityUpdate created;
	public CityUpdate destroyed;
}
