package Util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Parser {
	public static String configFolder = "Config/";
	public static String serverConfig = configFolder + "ServerConfig";
	public static String clientConfig = configFolder + "ClientConfig";
	public static String getString(String key, String file){
		key += ": ";
		file += ".txt";
		FileReader fr = null;
		try{
			fr = new FileReader(file);
			BufferedReader br = null;
			try{
				br = new BufferedReader(fr);
				for(String line; (line = br.readLine()) != null; ) {
					if(valid(line, key)){
						return line.substring(key.length(), line.length());
					}
				}
			} catch (IOException ioe){
				ioe.printStackTrace();
			} finally{
				if(br != null){
					try {
						br.close();
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
				}
			}
		} catch (IOException ioe){
			ioe.printStackTrace();
		} finally {
			if(fr != null){
				try {
					fr.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		return null;
	}
	private static Boolean valid(String line, String begin){
		return (begin.length() <= line.length()) && (line.substring(0, begin.length()).equals(begin));
	}
}
