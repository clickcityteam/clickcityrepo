package Util;

public class GridPosition {
	
	private int mRow;
	private int mCol;

	/*
	 * constructor
	 * @param posX
	 * @param posY
	 */
	public GridPosition(int row, int col)
	{
		mRow=row;
		mCol=col;
	}
	
	/*
	 * returns x position
	 */
	public int GetRow()
	{
		return mRow;
	}

	/*
	 * returns y position
	 */
	public int GetCol()
	{
		return mCol;
	}
}
