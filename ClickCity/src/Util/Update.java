package Util;

import java.io.Serializable;

public abstract class Update implements Serializable {
	private static final long serialVersionUID = 1L;
	private String sender;
	public String getSender(){
		return sender;
	}
	
	public void setSender(String sender){
		this.sender = sender;
	}
}
