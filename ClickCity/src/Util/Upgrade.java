package Util;

import java.io.Serializable;

public class Upgrade implements Serializable{
	private static final long serialVersionUID = 1L;

	public enum UpgradeType{
		clicker,
		passiveGenerator,
		city
	}

	private String mName;
	

	private UpgradeType mType;
	private float mBaseCost;
	private float mMultiplicativeCost;
	private float mEffect;
	private int amount;
	
	public Upgrade(UpgradeType type, String name, float baseCost, float mulitplicativeCost, float effect, int amount){
		this.mType = type;
		this.mName = name;
		this.mBaseCost = baseCost;
		this.mMultiplicativeCost = mulitplicativeCost;
		this.mEffect = effect;
		this.amount = amount;
	}
	public String getName() {
		return mName;
	}

	public UpgradeType getType() {
		return mType;
	}

	public float getBaseCost() {
		return mBaseCost;
	}

	public float getMultiplicativeCost() {
		return mMultiplicativeCost;
	}

	public float getEffect() {
		return mEffect;
	}
	
	public int getAmount(){
		return amount;
	}
};
