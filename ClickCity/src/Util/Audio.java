package Util;

import java.io.File;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
	
	/*
	 * Call this to play a sound 
	 * @param File: sound
	 */
	public static void PlaySound(File sound)
	{
		try {
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(sound));
			clip.start();
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
