package CustomGUI;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.Border;

import Util.Constants;

public class CButton extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8289096914061393799L;
	protected Image image;
	protected ImageIcon icon;
	
	protected String mName;
	protected Font mFont;
	
	protected ImageIcon mDefaultIcon;
	protected ImageIcon mHoverIcon;
	protected ImageIcon mPressedIcon;
	protected ImageIcon mDisabledIcon;
	
	protected Border raisedbevel;
	protected Border loweredbevel;
	
	protected boolean toggled;
	
	public CButton(String s)
	{	
		super(s);
		mName = s;
		mFont = new Font("KenVector Future Regular",Font.PLAIN, 12);
	}
	

	public CButton(int v)
	{
		mName = Integer.toString(v);
		setText(mName);
		mFont = new Font("KenVector Future Regular",Font.PLAIN, 12);
	}
	
	public boolean isToggled()
	{
		return toggled;
	}
	
	public void setFont(Font f)
	{
		mFont = f;
	}
	
	public void setString(String s)
	{
		mName = s;
		setText(s);
		this.repaint();
	}
	
	public void setDefaultIcon(ImageIcon icon)
	{
		mDefaultIcon = icon;
		icon = mDefaultIcon;
		image = icon.getImage();
		this.setBorder(raisedbevel);
	}
	
	public void setHoverIcon(ImageIcon icon)
	{
		mHoverIcon = icon;
		icon = mHoverIcon;
	}
	
	public void setPressedIcon(ImageIcon icon)
	{
		mPressedIcon = icon;
		icon = mPressedIcon;
	}
	
	public void setDisabledIcon(ImageIcon icon)
	{
		mDisabledIcon = icon;
		icon = mDisabledIcon;
	}
	
	public void setBorderEnabled(boolean b)
	{
		this.setBorderPainted(b);
	}
	
	public void hasRaisedBevelBorder(boolean b)
	{
		if(b)
		{
			raisedbevel = BorderFactory.createEtchedBorder();
		}
		else
		{
			raisedbevel = BorderFactory.createEmptyBorder();
			this.setBorder(raisedbevel);
		}
		
		repaint();
	}
	
	public void setRaisedBevelBorder(Border b)
	{
		raisedbevel = b;
		repaint();
	}
	
	public void setLoweredBevelBorder(Border b)
	{
		loweredbevel = b;
		repaint();
	}
	
	
	{
		toggled = false;
		
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(true);
		setFocusPainted(false);
		
		loweredbevel = BorderFactory.createLoweredBevelBorder();
		raisedbevel = BorderFactory.createEtchedBorder();
		
		this.setBorder(raisedbevel);
		
    	 mDefaultIcon = new ImageIcon(Constants.UIFolder + Constants.defaultButton);
	     mHoverIcon = new ImageIcon(Constants.UIFolder + Constants.defaultHoverButton);
    	 mDisabledIcon = new ImageIcon(Constants.UIFolder + Constants.defaultDisabledButton);
	     mPressedIcon = new ImageIcon(Constants.UIFolder + Constants.defaultPressedButton);
	     
	     image = mDefaultIcon.getImage();
	     icon = mDefaultIcon;
	     
		 this.setBorderPainted(true);
		 
		 this.addMouseListener(new java.awt.event.MouseAdapter() 
		 {
			 boolean isIn = false;
			    public void mouseEntered(java.awt.event.MouseEvent evt) 
			    {
					if(!isEnabled())
						return;
					
					image = mHoverIcon.getImage();
					icon = mHoverIcon;
					isIn = true;
					
					if(!toggled)
						setBorder(raisedbevel);
			    }
			    public void mousePressed(java.awt.event.MouseEvent evt)
			    {
					if(!isEnabled())
						return;
					
					image = mPressedIcon.getImage();
					icon = mPressedIcon;
					
					if(!toggled)
						setBorder(loweredbevel);
			    }
			    public void mouseReleased(java.awt.event.MouseEvent evt)
			    {
					if(!isEnabled())
						return;
					
			    	if(isIn == false)
			    		return;
			    	
					image = mHoverIcon.getImage();
					icon = mHoverIcon;
					
					if(!toggled)
						setBorder(raisedbevel);
			    }
			    public void mouseExited(java.awt.event.MouseEvent evt) 
			    {
					if(!isEnabled())
						return;
					
			    	if(toggled == true)
			    	{
			    		image = mHoverIcon.getImage();
			    		icon = mHoverIcon;
			    		return;
			    	}
			    	
					image = mDefaultIcon.getImage();
					icon = mDefaultIcon;
					isIn = false;
					setBorder(raisedbevel);
			    }
		});
	}
	
	public String getString()
	{
		return mName;
	}
	
	public void disableButton()
	{
		this.setEnabled(false);
		image = mDisabledIcon.getImage();
		icon = mDisabledIcon;
		this.setBorder(loweredbevel);
	}
	

	public void enableButton()
	{
		setEnabled(true);
		image = mDefaultIcon.getImage();
		icon = mDefaultIcon;
		this.setBorder(raisedbevel);
	}
	
	public void toggleButton(boolean b)
	{
		toggled = b;
		
		if(b == false)
		{
			image = mDefaultIcon.getImage();
			icon = mDefaultIcon;
			this.setBorder(raisedbevel);
			repaint();
		}
	}
	@Override
	public void paintComponent(Graphics g)
	{
		
	    g.drawImage(image,  0 , 0 , getWidth() , getHeight() , icon.getImageObserver());
		super.paintComponent(g);

	}
}
