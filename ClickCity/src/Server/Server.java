package Server;

public class Server {
	private ServerManager SM;
	private MySQLDriver msql;
	private ServerCommandLine SCL;
	public Server(){
		SCL = new ServerCommandLine(this, System.in, System.out);
		msql = new MySQLDriver();
		SM = new ServerManager(this);
	}
	public ServerManager getSM() {
		return SM;
	}
	public MySQLDriver getMsql() {
		return msql;
	}
	public ServerCommandLine getSCL() {
		return SCL;
	}
	public void close(){
		System.exit(0);
	}
	
	public static void main(String args[]){
		new Server();
	}
}
