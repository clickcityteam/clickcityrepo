package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import CustomGUI.CButton;
import CustomGUI.CLabel;

public class RegisterGUI extends JFrame{
	
	private static final long serialVersionUID = -3626966578305795968L;
	
	private JTextField usernameTextField;
	private JPasswordField passwordTextField;
	private CLabel usernameLabel;
	private CLabel passwordLabel;
	private CLabel titleLabel;
	private CButton loginButton;
	private JPanel mainPanel;
	private JPanel contents;
	private ClientManager cm;
	private BufferedImage image;
	
	public RegisterGUI(ClientManager cm){
		super("Register");
		
		this.cm = cm;
		
		try {                
			image = ImageIO.read(new File("resources/images/LoginRegister/cityRegister.png"));
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		
		instantiateComponents();
		createGUI();
		addActionAdapters();
		setLocationRelativeTo(null);
	}
	
	private void instantiateComponents(){
		mainPanel = new JPanel();
		contents = new JPanel(){
			private static final long serialVersionUID = 1L;

			@Override
			  protected void paintComponent(Graphics g) {

			    super.paintComponent(g);
			        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
			}
		};
		usernameTextField = new JTextField(20);
		passwordTextField = new JPasswordField(20);
		
		titleLabel = new CLabel("Register...");
		usernameLabel = new CLabel("Username: ");
		passwordLabel = new CLabel("Password: ");
		
		loginButton = new CButton("Register & Login");
		
	}
	
	private void createGUI(){
		setSize(500, 200);
		
		mainPanel.setLayout(new BorderLayout());
		add(mainPanel);
		
		mainPanel.add(contents);
		
		contents.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		
		c.gridy = 0;
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		contents.add(titleLabel, c);
		
		c.gridy = 1; 
		contents.add(usernameLabel, c);
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		contents.add(usernameTextField, c);
		
		c.gridy = 2;
		contents.add(passwordLabel, c);
		contents.add(Box.createRigidArea(new Dimension(5,0)));
		contents.add(passwordTextField, c);
		
		//c.gridwidth = ;
		c.gridx = 1;
		c.gridy = 3;
		
		loginButton.setPreferredSize(new Dimension(110, 20));
		contents.add(loginButton, c);
		
		getRootPane().setDefaultButton(loginButton);
	
		revalidate();
		repaint();
		
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	private void addActionAdapters(){
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				String user = usernameTextField.getText();
				String pass = encrypt(new String(passwordTextField.getPassword()));
				
				boolean check = true;
				
				for (int i = 0; i < user.length(); i++){
					if (!Character.isDigit(user.charAt(i)) && !Character.isLetter(user.charAt(i))){
						check = false;
					}
					
				}
				
				if (check == true){
					if (user.length() <= 11){
						if (!user.equals("god")){
					 		if (!user.equals("guest")){
						 		cm.register(user, pass);
					 		}
						}
					}
					else{
						if (user.equals("guest")){
							JOptionPane.showMessageDialog(null, "Username cannot be ''guest''!", "Login Failed", JOptionPane.ERROR_MESSAGE);
						}
						if (user.equals("god")){
							JOptionPane.showMessageDialog(null, "Username cannot be ''god''", "Login Failed", JOptionPane.ERROR_MESSAGE);
						}
						if (user.length() > 11){
							JOptionPane.showMessageDialog(null, "Username can be a max of 11 characters!", "Login Failed", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Username must be alphanumeric!", "Login Failed", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
	
	//Sam's encrypt function
		public String encrypt(String toEncrypt) {
	        try {
	            final MessageDigest digest = MessageDigest.getInstance("md5");
	            digest.update(toEncrypt.getBytes());
	            final byte[] bytes = digest.digest();
	            final StringBuilder sb = new StringBuilder();
	            for (int i = 0; i < bytes.length; i++) {
	                sb.append(String.format("%02X", bytes[i]));
	            }
	            return sb.toString().toLowerCase();
	        } catch (Exception exc) {
	            return "";
	        }
	    }
	
	public void notifySuccess(boolean flag){
		if (flag){
			JOptionPane.showMessageDialog(null, "You can now purchase a Property!", "Register Succeeded", JOptionPane.INFORMATION_MESSAGE);
			dispose();
		}
		else{
			JOptionPane.showMessageDialog(null, "Username already in use!", "Register Failed", JOptionPane.ERROR_MESSAGE);
		}
	}
		
	public static void main(String[] args){
		new RegisterGUI(null);
	}
}
