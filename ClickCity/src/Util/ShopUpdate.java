package Util;

import java.util.Vector;

public class ShopUpdate extends Update {
	private static final long serialVersionUID = 1L;
	
	private Vector<Upgrade> upgrades;
	
	public ShopUpdate(Vector<Upgrade> upgrades){
		this.upgrades = upgrades;
	}
	
	public Vector<Upgrade> getUpgrades(){
		return upgrades;
	}
}
