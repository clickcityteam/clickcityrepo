package Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import CustomGUI.CButton;
import Util.Audio;
import Util.Constants;

public class CityTileGUI extends CButton {
	private static final long serialVersionUID = 1L;
	CityTile mCityTile;
	CityMapGUI gui;

	public CityTileGUI(String s) {
		super(s);
		
		this.hasRaisedBevelBorder(false);
		this.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(gui.getIsLocked())
					return;
				
				Audio.PlaySound(new File("resources/audio/Button_Click_Off.wav"));
				gui.useTileAt(mCityTile.getCityRow(), mCityTile.getCityCol(), CityTileGUI.this);
			}
			
		});
	}
	
	CityTile GetCityTile()
	{
		return mCityTile;
	}
	
	public void setGUI(CityMapGUI gui)
	{
		this.gui = gui;
	}
	
	public void setOwner(String s)
	{
		if(!s.equals("null") && !s.equals("")){
			this.setText(s);
		}
		mCityTile.setOwner(s);
		
	}

	public void SetFactory(CityTile inTile)
	{
		mCityTile = inTile;
		setTier(inTile.getTier());
		if(!inTile.getOwner().equals("null") || !inTile.getOwner().equals(""))
		{
			setOwner(inTile.getOwner());
		}
	}
	
	public void updateFactory(int tier, String owner)
	{
		mCityTile.setOwner(owner);
		mCityTile.setTier(tier);
		setTier(tier);
	}
	
	public void setTier(int tier)
	{
		if(tier == 0)
		{
			this.setDefaultIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city0DefaultButton));
			this.setPressedIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city0PressedButton));
			this.setDisabledIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city0DisabledButton));
			this.setHoverIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city0HoverButton));
		}
		else if(tier == 1)
		{
			this.setDefaultIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city1DefaultButton));
			this.setPressedIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city1PressedButton));
			this.setDisabledIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city1DisabledButton));
			this.setHoverIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city1HoverButton));
		}
		else if(tier == 2)
		{
			this.setDefaultIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city2DefaultButton));
			this.setPressedIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city2PressedButton));
			this.setDisabledIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city2DisabledButton));
			this.setHoverIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city2HoverButton));
		}
		else if(tier == 3)
		{
			this.setDefaultIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city3DefaultButton));
			this.setPressedIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city3PressedButton));
			this.setDisabledIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city3DisabledButton));
			this.setHoverIcon(new ImageIcon(Constants.cityImagesFolder + Constants.city3HoverButton));
		}
		
		repaint();
	}
}
