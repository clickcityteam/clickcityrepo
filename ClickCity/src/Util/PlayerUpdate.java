package Util;

public class PlayerUpdate extends Update {
	private static final long serialVersionUID = 1L;

	private int mApples;
	private float mApplesPerSecond;
	private float mApplesPerClick;
	private float mCityBenefit;

	public PlayerUpdate(int apples, float applesPerSecond, float applesPerClick, float cityBenefit){
		this.mApples = apples;
		this.mApplesPerSecond = applesPerSecond;
		this.mApplesPerClick = applesPerClick;
		this.mCityBenefit = cityBenefit;
	}
	
	public int getApples() {
		return mApples;
	}

	public float getApplesPerSecond() {
		return mApplesPerSecond;
	}

	public float getApplesPerClick() {
		return mApplesPerClick;
	}
	
	public float getCityBenefit() {
		return mCityBenefit;
	}
	
}
