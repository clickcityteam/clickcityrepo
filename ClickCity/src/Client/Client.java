package Client;

import Util.Parser;

public class Client {
	public Client(){
		int port = new Integer(Parser.getString("port", Parser.clientConfig));
		String hostname = Parser.getString("IPAddress", Parser.clientConfig);
		new ClientManager(hostname, port);
	}
	
	public static void main(String[] args){
		new Client();
	}
}
