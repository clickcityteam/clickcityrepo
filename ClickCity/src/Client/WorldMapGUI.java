package Client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import CustomGUI.CButton;
import CustomGUI.CWorldTile;
import Util.ChatMessage;
import Util.GridPosition;
import Util.WorldUpdate;

public class WorldMapGUI extends JFrame implements ActionListener{
	public static final long serialVersionUID=1;

	private ClientManager mClientManager;
	
	//Map
	private JLabel mWorldMapLabel;
	private JScrollPane mWorldScrollPane;
	private JPanel mWorldPanel;
	private JPanel mWorldTilePanel;
	private ArrayList<ArrayList<CWorldTile>> mCWorldTiles;
	
	//Chat
	private JScrollPane mChatScrollPane;
	private JTextPane mChatTextPane;
	private StyledDocument doc;
	private ArrayList<String> memeList;
	private JPanel mChatPanel;
	private JPanel mChatFlowPanel;
	private JLabel mChatAreaLabel;
	private JTextField mChatMessageField;
	private CButton mChatSendBtn;
	private CButton mClearChatBtn;
	
	//Entire Window
	private int mHeight;
	private int mWidth;
	private JPanel infoPanel;
	private JPanel applePanel;
	private Image mAppleImage;
	private ImageIcon mAppleIcon;
	private JLabel totalAppleLabel, applePerSecondLabel, applePerClickLabel;
	
	/*
	 * constructor
	 * @param ClientManager: clientManager
	 */
	public WorldMapGUI(ClientManager clientManager, ArrayList<ArrayList<WorldTile>> worldTiles){
		//Audio.PlaySound(new File("resources/audio/NextEpisode.wav"));
		//Grab constructor arguements
		mClientManager= clientManager;
		
		//Initialize the Jframe
		mWidth=1280;
		mHeight=920;
		this.setBounds(Toolkit.getDefaultToolkit().getScreenSize().width/2-mWidth/2, 
				Toolkit.getDefaultToolkit().getScreenSize().height/2-mHeight/2, mWidth, mHeight);
		this.setTitle("World Map");
		this.setLayout(new BorderLayout());
		Initialize(worldTiles);
		CreateGUI();
		addActionListeners();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(false);
		setLocationRelativeTo(null);
	}
	
	private void Initialize(ArrayList<ArrayList<WorldTile>> worldTiles)
	{//mApplesImage

		mClearChatBtn= new CButton("Clear");
		mClearChatBtn.setPreferredSize(new Dimension(90,30));
		applePanel= new JPanel();
		infoPanel= new JPanel();
	    totalAppleLabel = new JLabel("Total Apples: 0"+"     ");
	    applePerSecondLabel = new JLabel("Apples per Second: 0"+"     ");
	    applePerClickLabel = new JLabel("Apple per Click: 0"+"     ");
		parseEmotes(); //this goes at top of initialize
		//Chat
		mChatPanel= new JPanel(new BorderLayout());
		mChatSendBtn=new CButton("Send");
		mChatSendBtn.setPreferredSize(new Dimension(120,30));
		mChatMessageField= new JTextField();
		mChatMessageField.setPreferredSize(new Dimension(200,30));
		mChatTextPane = new JTextPane();
		mChatTextPane.setEditable(false);
		doc = mChatTextPane.getStyledDocument();
		JPanel noWrapPanel = new JPanel( new BorderLayout() );
		noWrapPanel.add( mChatTextPane );	
		//mChatScrollPane= new JScrollPane(mChatTextPane);
		mChatScrollPane = new JScrollPane( noWrapPanel );
		mChatScrollPane.setViewportView(mChatTextPane);
		mChatScrollPane.setPreferredSize(new Dimension(100,200));
		mChatScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
		    public void adjustmentValueChanged(AdjustmentEvent e) {  
		        e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
		    }
		});

		mChatAreaLabel= new JLabel("Chat:");
		mChatFlowPanel= new JPanel(new FlowLayout());		

		//World Map
		mCWorldTiles=new ArrayList<ArrayList<CWorldTile>>();
		mWorldMapLabel= new JLabel("World Map: ");
		mWorldPanel= new JPanel(new BorderLayout());
		InitializeWorldTiles(worldTiles);
		mWorldScrollPane= new JScrollPane(mWorldTilePanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		Border bevelborder = BorderFactory.createLoweredBevelBorder();
		mWorldPanel.setBorder(bevelborder);
		
		//Set the client manager for each worldtile
		for(int i=0;i<worldTiles.size();i++){
			for(int k=0;k<worldTiles.size();k++){
				worldTiles.get(i).get(k).SetClientManager(mClientManager);
			}
		}
	}
	
	//Gets the apples from noah's client manager
	public void notifyPlayerChange()
	{
		    totalAppleLabel.setText("Total Apples: "+new Double(mClientManager.getApples()).toString()+"     ");
		    applePerSecondLabel.setText("Apples per Second: "+new Double(mClientManager.getApplesPerClick()).toString()+"     ");
		    applePerClickLabel.setText("Apple per Click: "+new Double(mClientManager.getApplesPerSecond()).toString()+"     ");	
	}
	
	private void CreateGUI()
	{
		mAppleIcon = new ImageIcon("resources/images/"+"apple.png");
	    mAppleImage = mAppleIcon.getImage();
		mWorldPanel.add(mWorldMapLabel);
		applePanel= new JPanel(){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g)
			{
			    g.drawImage(mAppleImage,  0 , 0 , getWidth() , getHeight() , mAppleIcon.getImageObserver());
			}
		};
		applePanel.setPreferredSize(new Dimension(40,40));
	    infoPanel.add(applePanel);
	    infoPanel.add(totalAppleLabel);
	    infoPanel.add(applePerSecondLabel);
	    infoPanel.add(applePerClickLabel);
	    add(infoPanel, BorderLayout.NORTH);
		//Create Chat Panel
		mChatPanel.add(mChatAreaLabel,BorderLayout.NORTH);
		mChatPanel.add(mChatScrollPane, BorderLayout.CENTER);
		
		//Chat Flow Layout
		mChatFlowPanel.add(mChatMessageField);
		mChatFlowPanel.add(mChatSendBtn);
		mChatFlowPanel.add(mClearChatBtn);
		mChatPanel.add(mChatFlowPanel, BorderLayout.SOUTH);
		this.add(mChatPanel,BorderLayout.EAST);
		
		//Create World Panel
		mWorldPanel.add(mWorldMapLabel,BorderLayout.NORTH);
		mWorldPanel.add(mWorldScrollPane,BorderLayout.CENTER);
		this.add(mWorldPanel, BorderLayout.CENTER);
	}
	
	/*
	 * Parse emotes function that makes emots
	 */
	private void parseEmotes(){
		memeList = new ArrayList<String>();
		File folder = new File("resources/images/Emotes");
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		        memeList.add(listOfFiles[i].getName());
		      } else if (listOfFiles[i].isDirectory()) {
		      }
		}
	}

	/*
	 * recieveMessaage from clientManager and add it to the message
	 * text area
	 */
	public void receiveMessage(ChatMessage chatMessage)
	{
		this.addMessageToTextPane(chatMessage.getSender(), chatMessage.getMessage());
	}
	
	/*
	 * Called by Client Manager to update this WorldMapGUI
	 */
	public void Update(WorldUpdate worldUpdate)
	{
		int gridX= worldUpdate.getX();//col
		int gridY=worldUpdate.getY();//row
		boolean successful=worldUpdate.successful();
		if(successful){
			updateTile(gridY,gridX,worldUpdate);
		}
	}


	private void updateTile(int row, int col,WorldUpdate worldUpdate)
	{
		mCWorldTiles.get(row).get(col).updateTile(worldUpdate);
	}
	
	/*
	 * Initialize the world map of tiles 
	 * @param int: rows
	 * @param int: cols
	 */
	private void InitializeWorldTiles(ArrayList<ArrayList<WorldTile>> worldTiles)
	{
		mWorldTilePanel= new JPanel(new GridBagLayout());
		mWorldTilePanel.setBorder(LineBorder.createBlackLineBorder());
		GridBagConstraints gbc = new GridBagConstraints();
		//Initialize the grid of tiles
		for(int i=0;i<worldTiles.size();i++)
		{
			ArrayList<CWorldTile> mTileRow=new ArrayList<CWorldTile>(); //make row of tiles
			for(int j=0;j<worldTiles.get(0).size();j++)
			{
				//Create a tile 
				WorldTile tile=worldTiles.get(i).get(j);
				CWorldTile tileGUI= new CWorldTile(tile,this);
				gbc.gridx=j*tile.GetWidth();
				gbc.gridy=i*tile.GetHeight();
				gbc.gridwidth=1;
				gbc.gridheight=1;
				mWorldTilePanel.add(tileGUI,gbc);
				mTileRow.add(tileGUI);
			}
			mCWorldTiles.add(mTileRow);//add row of tiles
		}
	}
	
	/*
	 * Add message to the message area! when message is recieved
	 */
	private void addMessageToTextPane(String username, String message)
	{
		String internalString = message.replaceAll("[^A-Za-z0-9]", " ");
		String[] words = internalString.split(" ");
		Style style = mChatTextPane.addStyle("I'm a Style", null);
		try
		{		
			String colorStr = String.format("#%X", username.hashCode());
			Color color = null;
			try{
				color = Color.decode(colorStr);
			} catch (NumberFormatException nfe){
				color = new Color(53, 192, 108);
			}
			StyleConstants.setForeground(style, color );
			doc.insertString(doc.getLength(), "("+username+"): ", style );
		} catch(Exception e) { System.out.println(e); }
		for (int i = 0; i < words.length; i++){
			if (memeList.contains(words[i] + ".png")){
				String meme = words[i];
				try
				{
					StyleConstants.setForeground(style, Color.BLACK );
					doc.insertString(doc.getLength(), message.substring(0, message.indexOf(meme)), null );
				    StyleContext context = new StyleContext();
				    javax.swing.text.Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
				    Icon icon = new ImageIcon("resources/images/Emotes/" + words[i] + ".png");
				    JLabel label = new JLabel(icon);
				    StyleConstants.setComponent(labelStyle, label);			    
				    doc.insertString(doc.getLength(), "Ignored", labelStyle); 
				    StyleConstants.setForeground(style, Color.BLACK );
				    //if meme is not at the end of message
				    if (message.indexOf(meme) + meme.length() <= message.length() - 1){
				    	doc.insertString(doc.getLength(), message.substring(message.indexOf(meme) + meme.length() + 1, message.length()) + '\n', null );
				    } else{
				    	doc.insertString(doc.getLength(), "" + '\n', null);
				    }
				    return;
				}
				catch(Exception e) { System.out.println(e); }
			}
		}
		try{
			StyleConstants.setForeground(style, Color.BLACK );
			doc.insertString(doc.getLength(), message+"\n", null );
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	/*
	 *Send message to all clients 
	 */
	private void sendMessageToAll()
	{
		if(mClientManager.getUsername().equals("guest")) return;
		String text= this.mChatMessageField.getText();
		if(!text.equals(""))
		{
			ChatMessage m= new ChatMessage(mClientManager.getUsername(),text);
			mClientManager.sendMessage(m);
			mChatMessageField.setText("");
		}
	}
	
	private void addActionListeners()
	{
		//Add listener for send button
		this.mChatSendBtn.addActionListener(this);

		this.mClearChatBtn.addActionListener(this);
		//Add listener for enter button
		this.mChatMessageField.addKeyListener(new KeyAdapter()
	    {
		      public void keyPressed(KeyEvent e)
		      {
		    	  if (e.getKeyCode()==KeyEvent.VK_ENTER){
		              sendMessageToAll();
		          }
		      }
	    });
	}
	
	/*
	 * show that you are unable to buy city
	 */
	private void showUnsuccessfulBuyDialog()
	{
		JOptionPane.showMessageDialog(this,
				"Not enough apples to purchase a city. Need 100,000 apples!",
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}

	private void showSuccessfulBuyDialog()
	{
			JOptionPane.showMessageDialog(this,
				"You spent 100,000 apples to purchase a city!",
				"Message",
				JOptionPane.INFORMATION_MESSAGE,
				new ImageIcon("resources/images/City_3/base.png"));
	}

	/*
	 * show that you bought the city or no if u didnt
	 */
	public void notifyBuyWorldTileSuccess(boolean success)
	{
		if(success)
		{
			showSuccessfulBuyDialog();
		}else
		{
			showUnsuccessfulBuyDialog();
		}
	}
	
	public static void main(String [] args)
	{
		ArrayList<ArrayList<WorldTile>> worldTiles= new ArrayList<ArrayList<WorldTile>>();//Initialize the grid of tiles
		for(int i=0;i<20;i++)
		{
			ArrayList<WorldTile> mTileRow=new ArrayList<WorldTile>(); //make row of tiles
			for(int j=0;j<20;j++)
			{
				WorldTile tile= new WorldTile(new GridPosition(i,j));
				mTileRow.add(tile);
			}
			worldTiles.add(mTileRow);//add row of tiles
			
		}
		WorldMapGUI wm=new WorldMapGUI(null,worldTiles);
		wm.addMessageToTextPane("Rob", "wassup boyz im chillin on the spirit plane");
		wm.addMessageToTextPane("Sam", "shut the fuck up Rob");
		wm.addMessageToTextPane("Rob", "No doubt boy");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==this.mClearChatBtn)
		{
			mChatTextPane.setText("");
		}
		if(e.getSource()==this.mChatSendBtn)
		{
			sendMessageToAll();
		}
	}
}
