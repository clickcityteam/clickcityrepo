package Util;

public class UpgradeUpdate extends Update {
	private static final long serialVersionUID = 1L;
	private String username;
	private String upgradeName;
	private boolean successful;
	
	public UpgradeUpdate(String username, String upgradeName){
		this.username = username;
		this.upgradeName = upgradeName;
		successful = false;
	}
	
	public String getUser(){
		return username;
	}
	
	public String getUpgradeName(){
		return upgradeName;
	}
	
	public boolean successful(){
		return successful;
	}
	
	public void succeed(){
		successful = true;
	}
}
