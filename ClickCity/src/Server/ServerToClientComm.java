package Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Util.ChatUpdate;
import Util.CityUpdate;
import Util.ClickUpdate;
import Util.InitUpdate;
import Util.LoginUpdate;
import Util.MadeCityUpdate;
import Util.MoveInUpdate;
import Util.PlayerInformation;
import Util.PlayerUpdate;
import Util.PropertyData;
import Util.RegisterUpdate;
import Util.RequestPlayerInformation;
import Util.RequestPlayerUpdate;
import Util.ShopUpdate;
import Util.Update;
import Util.UpgradeUpdate;
import Util.WorldUpdate;

public class ServerToClientComm extends Thread {
	private ServerManager SL;
	private Server S;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private AppleThread at;
	public ServerToClientComm(Socket socket, ServerManager SL, Server S){
		try{
			Socket s = socket;
			this.SL = SL;
			this.S = S;
			ois = new ObjectInputStream(s.getInputStream());
			oos = new ObjectOutputStream(s.getOutputStream());
			this.start();
		}catch(IOException ioe){
			try{
				if(oos != null){
					oos.close();
				}
			}catch(IOException ioe2){
				ioe.printStackTrace();
			}try{
				if(ois != null){
					ois.close();
				}
			}catch(IOException ioe2){
				ioe.printStackTrace();
			}
			SL.removeSTCC(this);
		}
	}

	@Override
	public void run(){
		try{
			while(true){
				Update u = (Update) ois.readObject();
				if(u instanceof InitUpdate){
					InitUpdate iu = (InitUpdate) u;
					synchronized(S.getMsql()){
						InitUpdate push = S.getMsql().fillInit(iu);
						pushObject(push);
					}
				}
				else if(u instanceof RequestPlayerUpdate){
					RequestPlayerUpdate rpu = (RequestPlayerUpdate) u;
					synchronized(S.getMsql()){
						PlayerUpdate pu = S.getMsql().getPlayerUpdate(rpu.getUsername());
						if(pu != null){
							rpu.fillPlayerUpdate(pu);
							pushObject(rpu);
						}
					}
				}
				else if(u instanceof ChatUpdate){
					ChatUpdate cu = (ChatUpdate) u;
					S.getSM().PushToAllClients(cu);
				}
				else if(u instanceof ShopUpdate){
					//nothing
				}
				else if(u instanceof WorldUpdate){
					WorldUpdate wu = (WorldUpdate) u;
					synchronized(S.getMsql()){
					boolean successful = S.getMsql().makeWorld(wu);
						if(successful){
							wu.succeed();
							WorldThread wt = new WorldThread(S, wu.getX(), wu.getY());
							S.getSM().addWorldThread(wt);
							S.getSM().PushToAllClients(wu);
							pushObject(new MadeCityUpdate(true, wu.getOwner()));
						}
					}
				}
				else if(u instanceof UpgradeUpdate){
					UpgradeUpdate uu = (UpgradeUpdate) u;
					synchronized(S.getMsql()){
						boolean successful = S.getMsql().addUpgrade(uu.getUser(), uu.getUpgradeName());
						if(successful){
							uu.succeed();
							ShopUpdate su = S.getMsql().getShopUpdate(uu.getUser());
							pushObject(su);
							PlayerUpdate pu = S.getMsql().getPlayerUpdate(uu.getUser());
							pushObject(pu);
						}
					}
				}
				else if(u instanceof CityUpdate){
					CityUpdate cu = (CityUpdate) u;
					synchronized(S.getMsql()){
						PropertyData pd = S.getMsql().moveToProperty(cu.getOccupant(), cu.getWorldX(), cu.getWorldY(), cu.getX(), cu.getY());
						MoveInUpdate miu = new MoveInUpdate();
						if(pd != null && pd.successful){
							miu.succeed();
							pushObject(miu);
							CityUpdate created = pd.created;
							CityUpdate destroyed = pd.destroyed;
							if(created != null){
								created.succeed();
							}
							if(destroyed != null){
								destroyed.succeed();
							}
							S.getSM().PushToAllClients(destroyed);
							S.getSM().PushToAllClients(created);
						}
						else{
							pushObject(miu);
						}
					}
				}
				else if(u instanceof PlayerUpdate){
					//nothing
				}
				else if(u instanceof RequestPlayerInformation){
					RequestPlayerInformation rpi = (RequestPlayerInformation) u;
					synchronized(S.getMsql()){
						PlayerUpdate pu = S.getMsql().getPlayerUpdate(rpi.getUsername());
						if(pu != null){
							PlayerInformation pi = new PlayerInformation(rpi.getUsername(), pu.getApples(), pu.getApplesPerSecond(), pu.getApplesPerClick(), pu.getCityBenefit());
							pushObject(pi);
						}
					}
				}
				else if(u instanceof ClickUpdate){
					ClickUpdate cu  = (ClickUpdate) u;
					synchronized(S.getMsql()){
						boolean clicked = S.getMsql().click(cu.getUsername(), cu.getPropertyOwner());
						if(clicked){
							PlayerUpdate pu = S.getMsql().getPlayerUpdate(cu.getUsername());
							pushObject(pu);
						}
					}
				}
				else if(u instanceof LoginUpdate){
					LoginUpdate lu = (LoginUpdate) u;
					synchronized(S.getMsql()){
						boolean authenticated = S.getMsql().authenticate(lu.getUsername(), lu.getHashedPassword());
						if(authenticated){
							lu.authenticate();
							at = new AppleThread(S, this, lu.getUsername());
						}
						pushObject(lu);
					}
				}
				else if(u instanceof RegisterUpdate){
					RegisterUpdate ru = (RegisterUpdate) u;
					synchronized(S.getMsql()){
						boolean userExists = S.getMsql().userExists(ru.getUsername());
						if(!userExists){
							boolean successfullRegister = S.getMsql().addUserPassword(ru.getUsername(), ru.getHashedPassword());
							boolean successfullPlayerInstance = S.getMsql().CreatePlayer(ru.getUsername(), 50, 0.0f, 1.0f, 0.0f);
							if(successfullRegister && successfullPlayerInstance){
								ru.WeRegistered();
								at = new AppleThread(S, this, ru.getUsername());
							}
						}
						pushObject(ru);
					}
				}
			}
		}catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}catch(IOException ioe){
			//ioe.printStackTrace();
		}finally{
			try{
				if(oos != null){
					oos.close();
				}
			}catch(IOException ioe){
				//ioe.printStackTrace();
			}try{
				if(ois != null){
					ois.close();
				}
			}catch(IOException ioe){
				//ioe.printStackTrace();
			}
			if(at != null){
				at.disconnect();
			}
			SL.removeSTCC(this);
		}
	}
	
	public void pushObject(Update u){
		if(u == null){
			return;
		}
		try {
			oos.writeObject(u);
			oos.flush();
		} catch (IOException e) {
			try{
				if(oos != null){
					oos.close();
				}
			}catch(IOException ioe){
				//ioe.printStackTrace();
			}try{
				if(ois != null){
					ois.close();
				}
			}catch(IOException ioe){
				//ioe.printStackTrace();
			}
			if(at != null){
				at.disconnect();
			}
			SL.removeSTCC(this);
		}
	}
}
