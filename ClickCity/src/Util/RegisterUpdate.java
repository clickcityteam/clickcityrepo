package Util;

public class RegisterUpdate extends Update {
	private static final long serialVersionUID = 1L;
	private String username;
	private String hashedPassword;
	private boolean succeeded;
	
	public RegisterUpdate(String username, String hashedPassword){
		this.username = username;
		this.hashedPassword = hashedPassword;
		this.succeeded = false;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getHashedPassword(){
		return hashedPassword;
	}
	
	public boolean succeeded(){
		return succeeded;
	}
	
	public void WeRegistered(){
		succeeded = true;
	}

}
