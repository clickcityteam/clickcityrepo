package Util;

public class WorldUpdate extends Update
{
	private static final long serialVersionUID = 1;
	
	private int x;
	private int y;
	private int tier;
	private String mOwner;
	private boolean successful;
	
	public WorldUpdate(int x, int y, String owner, int tier){
		this.x = x;
		this.y = y;
		this.tier = tier;
		this.mOwner = owner;
		this.successful = false;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getTier() {
		return tier;
	}

	public String getOwner() {
		return mOwner;
	}
	
	public boolean successful(){
		return successful;
	}
	
	public void succeed(){
		successful = true;
	}
	
}
