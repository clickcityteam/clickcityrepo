package Client;

import java.util.ArrayList;

import Util.CityUpdate;
import Util.GridPosition;
import Util.WorldUpdate;

public class World {
	private ArrayList<ArrayList<WorldTile> > allWorldTiles;
	private int worldRows;
	private int worldCols;
	private int cityRows;
	private int cityCols;
	
	//first two are the rows/cols of the WORLD MAP, second are the # of rows/cols in city once you click a world tile
	//the third are the height and width of the WORLD TILE 
	//NEED TO REMOVE GUI STUFF FROM THIS CLASS, PLS TO FIX SO I CAN FIX
	
	public World(int worldRows, int worldCols, int cityRows, int cityCols){
		
		this.worldRows = worldRows;
		this.worldCols = worldCols;
		this.cityRows = cityRows;
		this.cityCols = cityCols;
		
		allWorldTiles = new ArrayList<ArrayList<WorldTile> >();
		
		for (int i = 0; i < worldRows; i++){
			ArrayList<WorldTile> thing = new ArrayList<WorldTile>();
			for (int j = 0; j < worldCols; j++){
				GridPosition gp = new GridPosition(i,j);
				WorldTile wt = new WorldTile(gp);
				thing.add(wt);
								
				ArrayList<ArrayList<CityTile> > allCityTiles = new ArrayList<ArrayList<CityTile> >();
					
				for (int k = 0; k < cityRows; k++){
					ArrayList<CityTile> thing2 = new ArrayList<CityTile>();
					for (int l = 0; l < cityCols; l++){
						GridPosition gpCity = new GridPosition(k, l);
						thing2.add(new CityTile(gpCity, gp));
					}
					allCityTiles.add(thing2);
				}
				
				wt.setCityTiles(allCityTiles);
			}
			allWorldTiles.add(thing);
		}
	}
	
	public ArrayList<ArrayList<WorldTile> > getAllWorldTiles(){
		return allWorldTiles;
	}
	
	public void updateWorldTile(int worldX, int worldY, String owner, int tier){
		allWorldTiles.get(worldY).get(worldX).setOwner(owner);
		allWorldTiles.get(worldY).get(worldX).setTier(tier);
	}
	
	public void updateCityTile(int worldX, int worldY, int cityX, int cityY, String owner, int tier){
		allWorldTiles.get(worldY).get(worldX).getCityTiles().get(cityY).get(cityX).setOwner(owner);
		allWorldTiles.get(worldY).get(worldX).getCityTiles().get(cityY).get(cityX).setTier(tier);
	}
	
	public int getWorldRows(){
		return worldRows;
	}
	
	public int getWorldCols(){
		return worldCols;
	}
	
	public int getCityRows(){
		return cityRows;
	}
	
	public int getCityCols(){
		return cityCols;
	}
	
	public void Update(Object update){
		if (update instanceof CityUpdate){
			if (((CityUpdate) update).successful()){
				if (((CityUpdate) update).isCreating()){
					
					//gets the world tile that the city is in
					WorldTile wt = allWorldTiles.get(((CityUpdate) update).getWorldY()).get(((CityUpdate) update).getWorldX());
					
					//gets the array of city tiles for that worldtile
					ArrayList<ArrayList<CityTile> > wtCities = wt.getCityTiles();
					CityTile ct = wtCities.get(((CityUpdate) update).getY()).get(((CityUpdate) update).getX());
					
					//assigns city to owner
					ct.setOwner(((CityUpdate) update).getOccupant());
					ct.setTier(((CityUpdate) update).getLevel());
				}
				if (((CityUpdate) update).isRemoving()){
					//gets the world tile that the city is in
					WorldTile wt = allWorldTiles.get(((CityUpdate) update).getWorldY()).get(((CityUpdate) update).getWorldX());
					
					//gets the array of city tiles for that worldtile
					ArrayList<ArrayList<CityTile> > wtCities = wt.getCityTiles();
					CityTile ct = wtCities.get(((CityUpdate) update).getY()).get(((CityUpdate) update).getX());
					
					//removes owner from city
					ct.removeOwner();
				}
			}
		}
		
		
		//WILL FINISH THIS LATER
		if (update instanceof WorldUpdate){
			if (((WorldUpdate) update).successful()){
					
					//gets the world tile that the city is in
					WorldTile wt = allWorldTiles.get(((WorldUpdate) update).getY()).get(((WorldUpdate) update).getX());
					
					wt.setOwner(((WorldUpdate) update).getOwner());
					wt.setTier(((WorldUpdate) update).getTier());
				}
		}
	}
}
