package Client;

import Util.GridPosition;

public class CityTile {
	private int mTier;
	private String mUsername;
	private boolean hasOwner;
	
	private GridPosition worldPosition;
	private GridPosition cityPosition;
	
	public CityTile(GridPosition inCity, GridPosition inWorld)
	{
		worldPosition = inWorld;
		cityPosition = inCity;
		
		mUsername = "null";
		mTier = 0;
		hasOwner = false;
	}
	
	public int getCityRow()
	{
		return cityPosition.GetRow();
	}
	
	public int getWorldRow()
	{
		return worldPosition.GetRow();
	}
	
	public int getWorldCol()
	{
		return worldPosition.GetCol();
	}
	
	public int getCityCol()
	{
		return cityPosition.GetCol();
	}
	
	public void setOwner(String s)
	{
		mUsername = s;
		if(!s.equals("null") && !s.equals("")){
			hasOwner = true;
		}
		else{
			hasOwner = false;
		}
	}
	
	public void removeOwner()
	{
		mUsername = "null";
		hasOwner = false;
		mTier = 0;
	}
	
	public String getOwner()
	{
		return mUsername;
	}
	
	public boolean hasOwner()
	{
		return this.hasOwner;
	}
	
	public void setTier(int tier)
	{
		this.mTier = tier;
	}
	
	public int getTier()
	{
		return mTier;
	}

	public void setCoords(GridPosition gridPosition) {
		cityPosition = gridPosition;
		
	}
}
