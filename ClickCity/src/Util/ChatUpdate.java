package Util;

public class ChatUpdate extends Update{
	private static final long serialVersionUID = 1L;
	private ChatMessage cm;
	
	
	public ChatUpdate(ChatMessage cm){
		this.cm = cm;
	}
	
	public ChatMessage getChatMessage(){
		return cm;
	}
}
