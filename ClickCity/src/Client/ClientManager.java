package Client;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Util.ChatMessage;
import Util.CityUpdate;
import Util.InitUpdate;
import Util.MoveInUpdate;
import Util.PlayerInformation;
import Util.PlayerUpdate;
import Util.ShopUpdate;
import Util.Upgrade;
import Util.WorldUpdate;

public class ClientManager
{
	private String mUsername;
	private double mApples;
	private double mApplesPerSecond;
	private double mApplesPerClick;
	private World mWorld;
	
	private LoginGUI mLoginGUI;
	private RegisterGUI mRegisterGUI;
	private WorldMapGUI mWorldMapGUI;
	private CityMapGUI mCityMapGUI;
	private Shop mShop; //has reference to the GUI
	
	ClientToServerComm comm;
	
	public ClientManager(String hostname, int port)
	{
		comm = new ClientToServerComm(hostname, port, this);
		
		mUsername = "guest";
		
		mWorld = new World(80, 80, 4, 4);
		
		mLoginGUI = new LoginGUI(this);
		mRegisterGUI = new RegisterGUI(this);
		mWorldMapGUI = new WorldMapGUI(this, mWorld.getAllWorldTiles());
		mShop = new Shop(this);
				
		mLoginGUI.setVisible(true);
	}
	
	public void updateInitialValues(InitUpdate iu)
	{
		for (WorldUpdate wu : iu.getWorldUpdates())
		{
			mWorld.Update(wu);
			mWorldMapGUI.Update(wu);
		}
		
		for (CityUpdate cu : iu.getCityUpdates())
		{
			mWorld.Update(cu);
		}
		
		updatePlayer(iu.getPlayerUpdate());

		mShop.Update(iu.getShopUpdate());
	}
	
	public void updateWorldMap(WorldUpdate wu)
	{
		mWorld.Update(wu);
		mWorldMapGUI.Update(wu);
	}
	
	public void updateCityMap(CityUpdate cu)
	{
		mWorld.Update(cu);
		
		if (mCityMapGUI != null)
		{
			mCityMapGUI.Update(cu);
		}
	}
	
	public void updateShop(ShopUpdate s)
	{
		if (mShop != null)
		{
			mShop.Update(s);
		}
	}
	
	public void updatePlayer(PlayerUpdate p)
	{
		mApples = p.getApples();
		mApplesPerSecond = p.getApplesPerSecond();
		mApplesPerClick = p.getApplesPerClick();
		
		//apple cap checks
		if (mApples > 1000000000)
		{
			mApples = 1000000000;
		}
		if (mApplesPerSecond > 1000000000)
		{
			mApplesPerSecond = 1000000000;
		}
		if (mApplesPerClick > 1000000000)
		{
			mApplesPerClick = 1000000000;
		}
		
		//notify the GUIs that the apple totals have changed
		notifyGUIPlayerChange();
	}
	
	private void notifyGUIPlayerChange()
	{
		if (mShop != null && mWorldMapGUI != null)
		{
			mShop.notifyPlayerChange();
			mWorldMapGUI.notifyPlayerChange();
		}
		
		if (mCityMapGUI != null)
		{
			mCityMapGUI.notifyPlayerChange();
		}
	}
	
	public void login(String username, String password)
	{
		comm.login(username, password);
	}
	
	public void notifyLoginSuccess(String username, boolean succeeded)
	{
		mLoginGUI.notifySuccess(succeeded);
		if(succeeded)
		{
			mUsername = username;
			comm.requestInitUpdate();
			mWorldMapGUI.setVisible(true);
		}
	}
	
	public void register(String username, String password)
	{
		comm.register(username, password);
	}
	
	public void notifyRegisterSuccess(String username, boolean succeeded)
	{
		mUsername = username;
		mRegisterGUI.notifySuccess(succeeded);
		
		if (succeeded)
		{
			mWorldMapGUI.setEnabled(true);
			mCityMapGUI.setEnabled(true);
			mCityMapGUI.setVisible(true);
		}
	}
	
	public void playAsGuest()
	{
		mWorldMapGUI.setVisible(true);
		comm.requestInitUpdate();
		mWorldMapGUI.setVisible(true);
	}
	
	public void sendMessage(ChatMessage message)
	{
		comm.sendMessage(message);
	}
	
	public void receiveMessage(ChatMessage message)
	{
		if (mWorldMapGUI != null)
		{
			mWorldMapGUI.receiveMessage(message);
		}
	}
	
	public void buyTile(WorldTile worldTile)
	{
		comm.buyTile(worldTile);
	}
	
	public void notifyBuyWorldTileSuccess(boolean succeeded)
	{
		mWorldMapGUI.notifyBuyWorldTileSuccess(succeeded);
	}
	
	public void moveIntoCity(CityTile cityTile)
	{
		//from home city to new home city
		comm.moveIntoCity(cityTile);
	}
	
	public void notifyMoveInSuccess(MoveInUpdate miu)
	{
		mCityMapGUI.notifyMoveInSuccess(miu.succeeded());
	}
	
	public void requestCityTileApplesPerClick(CityTile ct)
	{
		comm.requestCityTileApplesPerClick(ct);
	}
	
	public void requestPlayerInformation(String username)
	{
		comm.requestPlayerInformation(username);
	}
	
	//called when the user clicks the apple-collecting button
	public void pickApples(CityTile cityTile)
	{
		if (cityTile.getOwner() != null)
		{
			if (mUsername.equals("guest"))
			{
				mApples++;
				notifyGUIPlayerChange();
				
				if (mApples >= 50)
				{
					mRegisterGUI.setVisible(true);
					mWorldMapGUI.setVisible(false);
					mCityMapGUI.setVisible(false);
				}
			}
			else
			{
				comm.pickApples(cityTile.getOwner());
			}
		}
	}
	
	public void buyUpgrade(Upgrade upgrade)
	{
		comm.buyUpgrade(upgrade);
	}
	
	public void notifyUpgradePurchase(Upgrade upgrade, boolean success)
	{
		mShop.notifyUpgradePurchase(upgrade, success);
	}
	
	public void openCity(WorldTile w)
	{
		mCityMapGUI = new CityMapGUI(this, w);
		mCityMapGUI.setVisible(true);
	}
	
	public String getUsername()
	{
		return mUsername;
	}
	
	public double getApples()
	{
		return mApples;
	}
	
	public double getApplesPerSecond()
	{
		return mApplesPerSecond;
	}
	
	public double getApplesPerClick()
	{
		return mApplesPerClick;
	}
	
	public World getWorld()
	{
		return mWorld;
	}
	
	public Shop getShop()
	{
		return mShop;
	}
	
	public WorldMapGUI getWorldMapGUI()
	{
		return mWorldMapGUI;
	}
	
	public void notifyCityTileApplesPerClick(double apc)
	{
		mCityMapGUI.notifyCityTileApplesPerClick(apc);
	}

	//called if client cannot connect to server
	public void AlertOffline()
	{
		JFrame dummyGUI = new JFrame();
		JOptionPane.showMessageDialog(dummyGUI, "Could not connect to the server; shutting down", "Connection failed...", JOptionPane.ERROR_MESSAGE);
		System.exit(0);
	}
	
	public void resetCityMapGUI()
	{
		mCityMapGUI = null;
	}

	public void updatePlayerInformation(PlayerInformation update) {
		if(mCityMapGUI != null)
		{
			mCityMapGUI.updatePlayerInformation(update);
		}
	}
}