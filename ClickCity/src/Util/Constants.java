package Util;

public class Constants {
	public static String resourcesFolder = "resources/";
	
	public static String defaultButton = "defaultbutton.jpg";
	public static String defaultPressedButton = "defaultbuttonclicked.jpg";
	public static String defaultDisabledButton = "defaultbuttondisabled.jpg";
	public static String defaultHoverButton = "defaultbuttonhover.jpg";
	
	public static String imagesFolder = "resources/images/";
	
	public static String UIFolder = "resources/images/UI/";
	
	public static String cityImagesFolder = "resources/images/city/";
	
	public static String city0DefaultButton = "city0defaultbutton.jpg";
	public static String city0PressedButton = "city0pressedbutton.jpg";
	public static String city0DisabledButton = "city0ddefaultbutton.jpg";
	public static String city0HoverButton = "city0hoverbutton.jpg";
	
	public static String city1DefaultButton = "city1defaultbutton.jpg";
	public static String city1PressedButton = "city1pressedbutton.jpg";
	public static String city1DisabledButton = "city1ddefaultbutton.jpg";
	public static String city1HoverButton = "city1hoverbutton.jpg";
	
	public static String city2DefaultButton = "city2defaultbutton.jpg";
	public static String city2PressedButton = "city2pressedbutton.jpg";
	public static String city2DisabledButton = "city2ddefaultbutton.jpg";
	public static String city2HoverButton = "city2hoverbutton.jpg";
	
	public static String city3DefaultButton = "city3defaultbutton.jpg";
	public static String city3PressedButton = "city3pressedbutton.jpg";
	public static String city3DisabledButton = "city3ddefaultbutton.jpg";
	public static String city3HoverButton = "city3hoverbutton.jpg";
	
}
