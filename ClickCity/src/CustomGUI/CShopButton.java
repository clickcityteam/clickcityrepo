package CustomGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import Util.Upgrade;

public class CShopButton extends CButton {
	private static final long serialVersionUID = 1L;
	protected Image image;
	protected ImageIcon icon;
	
	private int mCount;
	private boolean mIsBought;
	private String mName;
	private Upgrade mUpgrade;
	
	private int mWidth, mHeight;
	
	public CShopButton(String name, int preferedWidth, int preferedHeight, Upgrade upgrade){
		super("");
		mUpgrade = upgrade;
		mName = name;
		mWidth = preferedWidth;
		mHeight = preferedHeight;
		this.setPreferredSize(new Dimension(preferedWidth, preferedHeight));		
	}
	
	public String getUpgradeName(){
		return mUpgrade.getName();
	}
	
	public Upgrade getUpgrade(){
		return mUpgrade;
	}
	
	public void upgrade(){
		if (mIsBought){
			mCount++;
		} else {
			mIsBought = true;
			mCount++;
		}
	}
	
	public int getCount(){
		return mCount;
	}
	
	@Override
	public int getWidth(){
		return mWidth;
	}
	
	@Override
	public int getHeight(){
		return mHeight;
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
        
        int start = 30;
        g.drawString(mName, start, 45);
        
        //DRAW THE UPDATES
        g.setColor(Color.RED);
        int size = 10;
        int apart = 14;
        int startYSquare = 55;
        int softLimit = -1;

        for (int i=0; i < mUpgrade.getAmount(); i++){
        	softLimit++;
        	if (start + softLimit*apart + size <= (getWidth() - getWidth()/4)){
        		g.fillRect(start + softLimit*apart,  startYSquare, size, size);
        	} else {
        		softLimit = 0;
        		startYSquare += 15;
        		g.fillRect(start + softLimit*apart,  startYSquare, size, size);
        	}
        }
        
        //DRAW THE UPGRADEABLE
        g.setColor(Color.BLACK);
        g.drawLine(getWidth() - getWidth()/4, 0, getWidth() - getWidth()/4, getHeight());
        
        //DRAW STATUS
        String status = "";
        if (mUpgrade.getAmount() == 0){
        	status = "BUY";
        	int startX = getWidth() - getWidth()/8 - status.length()/2 - 8;
        	g.drawString(status, startX, 50);
        } else {
        	status = "UPGRADE";
        	int startX = getWidth() - getWidth()/8 - status.length()/2 - 16;
        	g.drawString(status, startX, 45);
        	g.drawString("X" + Integer.toString(mUpgrade.getAmount()), getWidth() - getWidth()/4 + getWidth()/8 - 1, 65);
        }
	}
}
