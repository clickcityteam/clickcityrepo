package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import CustomGUI.CButton;
import Client.WorldTile;
import Util.Audio;
import Util.CityUpdate;
import Util.PlayerInformation;

public class CityMapGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	ClientManager mClientManager;
	Shop mShop;
	Vector<String> mPlayerArray;
	ArrayList<ArrayList<CityTileGUI>> mCityTileGUIArray;
	ArrayList<ArrayList<CityTile>> mCityTileArray;
	WorldTile mWorldTile;

	JTextArea playerInformation;

	CButton WorldButton;
	CButton ShopButton;
	JPanel PlayerPanel;

	CButton CollectButton;

	JPanel mMapPanel;
	JPanel mSidePanel;

	JLabel InfoPanel;

	JTextArea playerText;

	double applesPerClick;

	boolean ownsShop;

	boolean lockedOnTile;

	CityTileGUI selectedTileGUI;

	final static int N = 4;

	CityMapGUI(ClientManager inClientManager, WorldTile inWorldTile) {
		lockedOnTile = false;
		ownsShop = false;
		this.setResizable(false);

		mClientManager = inClientManager;
		mShop = mClientManager.getShop();
		mWorldTile = inWorldTile;
		mCityTileArray = mWorldTile.getCityTiles();

		setTitle("City Map");
		setVisible(false);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mClientManager.getWorldMapGUI().setVisible(true);
				mClientManager.getShop().closeShop();
				mClientManager.resetCityMapGUI();
				dispose();
			}
		});
		setSize(800, 590);
		setPreferredSize(new Dimension(790, 540));
		setLocationRelativeTo(null);

		setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		c2.gridx = 0;
		c2.gridy = 0;
		c2.weightx = 0.5;
		c2.weighty = 0.5;

		mMapPanel = new JPanel();
		mMapPanel.setSize(400, 400);
		mMapPanel.setPreferredSize(new Dimension(400, 400));
		mMapPanel.setLayout(new GridLayout(N, N));

		mCityTileGUIArray = new ArrayList<ArrayList<CityTileGUI>>();

		int row = 0;
		int col = 0;
		for (int i = 0; i < N; i++) {
			ArrayList<CityTileGUI> thing = new ArrayList<CityTileGUI>();
			for (int j = 0; j < N; j++) {
				thing.add(null);
			}
			mCityTileGUIArray.add(thing);
		}

		ShopButton = new CButton("Shop");
		ShopButton.setPreferredSize(new Dimension(100, 50));
		ShopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!mShop.equals(null)) {
					mShop.openShop();
					Audio.PlaySound(new File("resources/audio/Button_Click_On.wav"));
				}
			}

		});

		ShopButton.disableButton();

		for (int i = 0; i < N * N; i++) {
			CityTileGUI temp;
			temp = new CityTileGUI("");
			temp.setGUI(this);

			mCityTileGUIArray.get(row).set(col, temp);
			temp.SetFactory(mCityTileArray.get(row).get(col));

			if (temp.GetCityTile().getOwner().equals(mClientManager.getUsername())) {
				ShopButton.enableButton();
				ownsShop = true;
			}

			mMapPanel.add(temp);

			if (col == N - 1) {
				col = 0;
				row++;
			} else {
				col++;
			}
		}

		Border bevelborder = BorderFactory.createLoweredBevelBorder();
		mMapPanel.setBorder(bevelborder);
		add(mMapPanel, c2);

		mSidePanel = new JPanel();
		mSidePanel.setSize(250, 400);
		mSidePanel.setPreferredSize(new Dimension(250, 400));
		mSidePanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.5;
		c.weighty = 0.5;

		WorldButton = new CButton("World Map");
		WorldButton.setPreferredSize(new Dimension(100, 50));
		WorldButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Audio.PlaySound(new File("resources/audio/Button_Click_On.wav"));
				mClientManager.getWorldMapGUI().setVisible(true);
				mClientManager.getShop().closeShop();
				mClientManager.resetCityMapGUI();
				dispose();
			}

		});

		PlayerPanel = new JPanel(new BorderLayout());
		TitledBorder playerTitle;
		playerTitle = BorderFactory.createTitledBorder("Players in this city:");
		PlayerPanel.setBorder(playerTitle);
		playerText = new JTextArea("");
		playerText.setEditable(false);
		playerText.setLineWrap(true);
		playerText.setWrapStyleWord(true);
		playerText.setPreferredSize(new Dimension(240, 70));
		PlayerPanel.add(playerText, BorderLayout.CENTER);

		c.anchor = GridBagConstraints.WEST;
		mSidePanel.add(WorldButton, c);
		c.gridy = 1;
		mSidePanel.add(ShopButton, c);
		c.gridy = 2;
		mSidePanel.add(PlayerPanel, c);

		c.gridx = 0;
		c.gridy = 3;
		JPanel PlayerInfoPanel = new JPanel(new BorderLayout());
		PlayerInfoPanel.setPreferredSize(new Dimension(250, 105));
		TitledBorder infoTitle;
		infoTitle = BorderFactory.createTitledBorder("Tile Information");
		playerInformation = new JTextArea("Click on a tile to see its information.");
		PlayerInfoPanel.setBorder(infoTitle);
		playerInformation.setEditable(false);
		playerInformation.setPreferredSize(new Dimension(240, 95));
		PlayerInfoPanel.add(playerInformation, BorderLayout.CENTER);
		mSidePanel.add(PlayerInfoPanel, c);

		c2.weighty = 1.0f;
		c2.gridx = 1;
		add(mSidePanel, c2);

		c2.gridx = 0;
		c2.gridy = 1;
		c2.gridwidth = 2;

		CollectButton = new CButton("(Select a tile first)");
		CollectButton.setPreferredSize(new Dimension(500, 60));
		CollectButton.disableButton();

		CollectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (selectedTileGUI.GetCityTile().hasOwner()) {
					Audio.PlaySound(new File("resources/audio/ping-bing.wav"));
					mClientManager.pickApples(selectedTileGUI.GetCityTile());
					
				} else {
					mClientManager.moveIntoCity(selectedTileGUI.GetCityTile());
					Audio.PlaySound(new File("resources/audio/Button_Click_On.wav"));

				}
			}

		});

		add(CollectButton, c2);
		c2.gridy = 2;

		InfoPanel = new JLabel();
		InfoPanel.setAlignmentX(CENTER_ALIGNMENT);
		InfoPanel.setText("Total Apples: 0     Apples/s: 0");

		add(InfoPanel, c2);

		this.updatePlayerVector();

		setVisible(true);

	}

	public void notifyPlayerChange() {
		InfoPanel.setText("Total Apples: " + (int) mClientManager.getApples() + "     Apples/s: "
				+ (int) mClientManager.getApplesPerSecond());
		if (selectedTileGUI == null)
			return;

		if (selectedTileGUI.GetCityTile().getOwner().equals(mClientManager.getUsername())) {
			CollectButton.setText("Collect " + (int) mClientManager.getApplesPerClick() + " from "
					+ mClientManager.getUsername() + "!");
		}
	}

	public boolean getIsLocked() {
		return lockedOnTile;
	}

	public void setIsLocked(boolean b) {
		lockedOnTile = b;
	}

	public void Update(CityUpdate inUpdate) {
		if (inUpdate.getWorldX() == mWorldTile.getCol() && inUpdate.getWorldY() == mWorldTile.getRow()) {
			CityTileGUI gui = mCityTileGUIArray.get(inUpdate.getY()).get(inUpdate.getX());

			if (inUpdate.isRemoving()) {
				gui.updateFactory(0, "null");
				gui.setText("");
				gui.setTier(0);

				if (selectedTileGUI == null)
					return;

				if (inUpdate.getY() == selectedTileGUI.GetCityTile().getCityRow()
						&& inUpdate.getX() == selectedTileGUI.GetCityTile().getCityCol()) {
					removePlayerInformation();
					selectedTileGUI.toggleButton(false);
				}
			} else if (inUpdate.isCreating()) {
				gui.setOwner(inUpdate.getOccupant());
				gui.updateFactory(inUpdate.getLevel(), inUpdate.getOccupant());

				if (selectedTileGUI == null)
					return;

				if (inUpdate.getY() == selectedTileGUI.GetCityTile().getCityRow()
						&& inUpdate.getX() == selectedTileGUI.GetCityTile().getCityCol()) {
					lockButton(selectedTileGUI, selectedTileGUI.GetCityTile().getCityRow(),
							selectedTileGUI.GetCityTile().getCityCol());
					selectedTileGUI.toggleButton(false);

				}
			}

			updatePlayerVector();
		}
	}

	public void updatePlayerVector() {
		mPlayerArray = new Vector<String>();
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (mCityTileGUIArray.get(i).get(j).GetCityTile().hasOwner()) {
					String player = mCityTileGUIArray.get(i).get(j).GetCityTile().getOwner();
					if (!player.equals("null") && !player.equals(""))
						mPlayerArray.addElement(player);
				}
			}
		}

		playerText.setText("");

		for (int i = 0; i < mPlayerArray.size(); i++) {
			if (i == mPlayerArray.size() - 1) {
				playerText.append(mPlayerArray.elementAt(i));
			} else {
				playerText.append(mPlayerArray.elementAt(i) + ", ");
			}
		}
	}

	public void notifyMoveInSuccess(boolean b) {
		if (b) {
			JOptionPane.showMessageDialog(this, "Successfully bought City Tile!");

			ShopButton.enableButton();

			ownsShop = true;
		} else {
			if (mClientManager.getUsername().equals("guest")) {
				JOptionPane.showMessageDialog(this, "Failed to buy City Tile. You need 50 apples.", "Error!",
						JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this, "Failed to buy City Tile. You need 25,000 apples.", "Error!",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		lockedOnTile = false;
	}

	public void notifyCityTileApplesPerClick(double apc) {
		if (mClientManager.getUsername().equals("guest")) {
			CollectButton.setString("Collect " + 1 + " apple from " + selectedTileGUI.GetCityTile().getOwner() + "!");
			setIsLocked(false);
			CollectButton.enableButton();
			return;
		}

		applesPerClick = apc;
		CollectButton
				.setString("Collect " + (int) apc + " apples from " + selectedTileGUI.GetCityTile().getOwner() + "!");
		CollectButton.enableButton();

		setIsLocked(false);
	}

	public void useTileAt(int row, int col, CityTileGUI button) {

		if (lockedOnTile)
			return;

		if (selectedTileGUI != null && selectedTileGUI != button)
			selectedTileGUI.toggleButton(false);

		selectedTileGUI = button;

		applesPerClick = 0;

		if (mCityTileGUIArray.get(row).get(col).GetCityTile().hasOwner()) {
			mClientManager.requestPlayerInformation(mCityTileGUIArray.get(row).get(col).GetCityTile().getOwner());
			if (!mCityTileGUIArray.get(row).get(col).isToggled()) {
				lockButton(button, row, col);
			}
		} else if (mClientManager.getUsername().equals("guest")) {
			removePlayerInformation();

			CollectButton.setString("Purchase tile for 50 apples.");
			CollectButton.enableButton();

			if (!selectedTileGUI.isToggled())
				button.toggleButton(true);
		} else {
			removePlayerInformation();

			CollectButton.setString("Purchase tile.");
			CollectButton.enableButton();

			if (!selectedTileGUI.isToggled())
				button.toggleButton(true);
		}
	}

	public void lockButton(CityTileGUI button, int row, int col) {
		CollectButton.setString("Retrieving apples/click from server...");
		mClientManager.requestCityTileApplesPerClick(mCityTileGUIArray.get(row).get(col).GetCityTile());
		CollectButton.disableButton();
		lockedOnTile = true;

		button.toggleButton(true);

	}

	public void updatePlayerInformation(PlayerInformation update) {
		String username = update.getUsername();
		int wallet = update.getApples();
		int applesclick = (int) update.getApplesPerClick();
		int applessecond = (int) update.getApplesPerSecond();
		int citybenefits = (int) update.getCityBenefit();

		playerInformation.setText("Username: " + username + "\n" + "Total Apples: " + wallet + "\n" + "Apples/click: "
				+ applesclick + "\n" + "Apples/second: " + applessecond + "\n" + "City Benefits: " + citybenefits);
	}

	public void removePlayerInformation() {
		playerInformation.setText("This tile is empty!");
	}

}
