package Util;

import java.util.ArrayList;

public class InitUpdate extends Update {
	private static final long serialVersionUID = 1L;
	private ArrayList<WorldUpdate> mWorldUpdates;
	private ArrayList<CityUpdate> mCityUpdates;
	private PlayerUpdate mPlayerUpdate;
	private ShopUpdate mShopUpdate;
	private String username;
	
	public InitUpdate(ArrayList<WorldUpdate> worldUpdates, ArrayList<CityUpdate> cityUpdates, PlayerUpdate playerUpdate, ShopUpdate shopUpdate, String username){
		this.mWorldUpdates = worldUpdates;
		this.mCityUpdates = cityUpdates;
		this.mPlayerUpdate = playerUpdate;
		this.mShopUpdate = shopUpdate;
		this.username = username;
	}

	public ArrayList<WorldUpdate> getWorldUpdates() {
		return mWorldUpdates;
	}

	public ArrayList<CityUpdate> getCityUpdates() {
		return mCityUpdates;
	}

	public PlayerUpdate getPlayerUpdate() {
		return mPlayerUpdate;
	}

	public ShopUpdate getShopUpdate() {
		return mShopUpdate;
	}
	
	public String getUsername(){
		return username;
	}
}
