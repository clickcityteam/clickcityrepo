package Util;

public class MoveInUpdate extends Update {
	private static final long serialVersionUID = 1L;
	private boolean successful;
	
	public MoveInUpdate(){
		successful = false;
	}
	
	public boolean succeeded(){
		return successful;
	}
	
	public void succeed(){
		successful = true;
	}
}
