package Server;

import java.io.InputStream;
import java.io.PrintStream;
import java.security.MessageDigest;
import java.util.Scanner;

import Util.ChatMessage;
import Util.ChatUpdate;
import Util.PlayerUpdate;

public class ServerCommandLine extends Thread {
	private Server S;
	private InputStream is;
	private PrintStream ps;
	public ServerCommandLine(Server S, InputStream is, PrintStream ps){
		this.S = S;
		this.is = is;
		this.ps = ps;
		this.start();
	}

	@Override
	public void run(){
		ps.println("Server started");
		Scanner s = null;
		try{
			s = new Scanner(is);
			while(true){
				String command = s.nextLine();
				ParseCommand(command);
			}
		} finally {
			if(s != null){
				s.close();
			}
		}
	}

	//Sam's encrypt function
	private String encrypt(String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return "";
        }
    }
	
	private String getWord(String line){
		String output = "";
		for(int i = 0; i < line.length(); i++){
			char c = line.charAt(i);
			if(c == ' '){
				break;
			}
			else{
				output += c;
			}
		}
		return output;
	}
	
	private String getLastWords(String line){
		boolean needStuff = true;
		boolean gonnaEnd = false;
		int start = 0;
		for(int i = 0; i < line.length(); i++){
			char c = line.charAt(i);
			if(gonnaEnd && !needStuff){
				start = i;
				break;
			}
			else if(needStuff && c != ' '){
				needStuff = false;
			}
			else if(!needStuff && c == ' '){
				gonnaEnd = true;
			}
		}
		return line.substring(start);
	}
	
	private void ParseCommand(String command) {
		final String test = "Test";
		final String chat = "Chat";
		final String stop = "Stop";
		String firstCommand = getWord(command);
		String otherStuff = getLastWords(command);
		if(firstCommand.equals(test)){
			TestCommand(otherStuff);
		}
		else if(firstCommand.equals(chat)){
			ChatCommand(otherStuff);
		}
		else if(firstCommand.equals(stop)){
			Stop();
		}
		else{
			ps.println("<Unrecognized Command>: " + firstCommand);
		}
		
	}

	private void Stop() {
		ps.println("Server shutting down...");
		S.close();
		
	}

	private void TestCommand(String command) {
		final String userpasscombo = "UserPassCombo";
		final String register = "Register";
		final String click = "Click";
		final String stats = "Stats";
		String firstCommand = getWord(command);
		String otherStuff = getLastWords(command);
		if(firstCommand.equals(userpasscombo)){
			AttemptLogin(otherStuff);
		}
		else if(firstCommand.equals(register)){
			Register(otherStuff);
		}
		else if(firstCommand.equals(click)){
			Click(otherStuff);
		}
		else if(firstCommand.equals(stats)){
			Stats(otherStuff);
		}
		else{
			ps.println("Unknown command for Test");
		}
	}

	private void Click(String otherStuff) {
		String username = getWord(otherStuff);
		boolean clicked = false;
		synchronized(S.getMsql()){
			clicked = S.getMsql().click(username, username);
		}
		ps.println("User: " + username);
		if(clicked){
			ps.println("Clicked!");
		}
		else{
			ps.println("User doesn't exist!");
		}
	}

	private void Stats(String otherStuff) {
		String username = getWord(otherStuff);
		String command = getWord(getLastWords(otherStuff));
		PlayerUpdate pu = null;
		synchronized(S.getMsql()){
			pu = S.getMsql().getPlayerUpdate(username);
		}
		ps.println("User: " + username);
		if(pu == null){
			ps.print("User doesn't exist!");
		}
		else if(command.equals("Apples")){
			ps.println("Apples: " + pu.getApples());
		}
		else if(command.equals("ApplesPerSecond")){
			ps.println("Apples/Second: " + pu.getApplesPerSecond());
		}
		else if(command.equals("ApplesPerClick")){
			ps.println("Apples/Click: " + pu.getApplesPerClick());
		}
		else if(command.equals("CityBenefit")){
			ps.println("City Benefit: " + pu.getCityBenefit());
		}
		else{
			ps.println("Unknown command for Stats!");
		}
		
	}

	private void AttemptLogin(String otherStuff) {
		String username = getWord(otherStuff);
		String password = getWord(getLastWords(otherStuff));
		boolean userExists = false;
		boolean successful = false;
		synchronized(S.getMsql()){
			userExists = S.getMsql().userExists(username);
			successful = S.getMsql().authenticate(username, encrypt(password));
		}
		
		if(successful){
			ps.println("User: " + username);
			ps.println("Password: " + password);
			ps.println("Authenticated!");
		}
		else if (userExists){
			ps.println("User: " + username);
			ps.println("Password: " + password);
			ps.println("User exists, but not authenticated.");
		}
		else{
			ps.println("User: " + username);
			ps.println("Doesn't exist.");
		}
	}
	
	private void Register(String otherStuff){
		String username = getWord(otherStuff);
		String password = getWord(getLastWords(otherStuff));
		boolean userExists = false;
		synchronized(S.getMsql()){
			userExists = S.getMsql().userExists(username);
		}
		if(!userExists){
			boolean successful = false;
			synchronized(S.getMsql()){
				successful = S.getMsql().addUserPassword(username, encrypt(password));
			}
			if(successful){
				ps.println("User: " + username);
				ps.println("Password: " + password);
				ps.println("Successfully registered!");
			}
		}
		else{
			ps.println("User: " + username);
			ps.println("User already exists!");
		}
		
	}

	private void ChatCommand(String command) {
		ChatMessage cm = new ChatMessage("Server", command);
		ChatUpdate cu = new ChatUpdate(cm);
		S.getSM().PushToAllClients(cu);
	}
}
