package Util;

public class RequestPlayerUpdate extends Update {
	private static final long serialVersionUID = 1L;
	private String username;
	private PlayerUpdate pu;
	public RequestPlayerUpdate(String username){
		this.username = username;
	}
	
	public void fillPlayerUpdate(PlayerUpdate pu){
		this.pu = pu;
	}
	
	public String getUsername(){
		return username;
	}
	
	public PlayerUpdate getPlayerUpdate(){
		return pu;
	}
}
