package Util;

public class PlayerInformation extends Update {
	private static final long serialVersionUID = 1L;

	private int mApples;
	private float mApplesPerSecond;
	private float mApplesPerClick;
	private float mCityBenefit;
	private String mUsername;

	public PlayerInformation(String username, int apples, float applesPerSecond, float applesPerClick, float cityBenefit){
		this.mUsername = username;
		this.mApples = apples;
		this.mApplesPerSecond = applesPerSecond;
		this.mApplesPerClick = applesPerClick;
		this.mCityBenefit = cityBenefit;
	}
	
	public int getApples() {
		return mApples;
	}

	public float getApplesPerSecond() {
		return mApplesPerSecond;
	}

	public float getApplesPerClick() {
		return mApplesPerClick;
	}
	
	public float getCityBenefit() {
		return mCityBenefit;
	}
	
	public String getUsername() {
		return mUsername;
	}
	
}
