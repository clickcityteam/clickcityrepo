package Client;

import java.util.ArrayList;

import Util.GridPosition;

public class WorldTile {

	private int mWidth;
	private int mHeight;
	private GridPosition mGridPosition;
	private ArrayList<ArrayList<CityTile>> mCityTiles;
	private boolean mIsOwned;
	private ClientManager mClientManager;
	private int mTier;
	private String mOwner;
	
	public WorldTile(GridPosition gridPosition)
	{
		mGridPosition=gridPosition;
		mWidth=80;
		mHeight=80;
		mIsOwned=false;
		mTier=1;
		mCityTiles = new ArrayList<ArrayList<CityTile>>();
		mOwner="nobody";
	}
	
	public void SetClientManager(ClientManager clientManager)
	{
		mClientManager=clientManager;
	}
	
	/*
	 * set tile tier
	 */
	public void setTier(int tier)
	{
		mTier=tier;
	}

	/*
	 * Set City tiles
	 */
	public void setCityTiles(ArrayList<ArrayList<CityTile>> cityTiles)
	{
		mCityTiles=cityTiles;
	}

	/*
	 * Get City tiles
	 */
	public ArrayList<ArrayList<CityTile>> getCityTiles()
	{
		return mCityTiles;
	}

	/*
	 * returns the client manager
	 */
	public ClientManager getClientManager()
	{
		return mClientManager;
	}
	
	/*
	 * returns true if tile is owned
	 */
	public boolean isOwned()
	{
		return mIsOwned;
	}
	
	/*
	 *returns the col of the world tile 
	 */
	public int getCol()
	{
		return mGridPosition.GetCol();
	}

	/*
	 *returns the row of the world tile 
	 */
	public int getRow()
	{
		return mGridPosition.GetRow();
	}
	
	/*
	 * Get the owner of this tile
	 */
	public String getOwnerName()
	{
		return this.mOwner;
	}
	
	/*
	 * Sets the owner of this tile
	 */
	public void setOwner(String username)
	{
		this.mOwner=username;
		this.mIsOwned=true;
	}
	
	/*
	 * returns the grid position of this world tile
	 */
	public GridPosition getGridPosition()
	{
		return mGridPosition;
	}

	/*
	 * returns the width of button
	 */
	public int GetWidth()
	{
		return mWidth;
	}

	/*
	 * returns the width of button
	 */
	public int GetHeight()
	{
		return mHeight;
	}
	
	public int GetTier()
	{
		return mTier;
	}
}
