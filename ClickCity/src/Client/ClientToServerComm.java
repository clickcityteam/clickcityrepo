package Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Util.*;

public class ClientToServerComm extends Thread
{
	private ClientManager CM;
	private String hostname;
	private int port;
	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	public ClientToServerComm(String hostname, int port, ClientManager CM)
	{
		this.hostname = hostname;
		this.port = port;
		this.CM = CM;
		try
		{
			s = new Socket(this.hostname, this.port);
			oos = new ObjectOutputStream(s.getOutputStream());	
			ois = new ObjectInputStream(s.getInputStream());
			this.start();
		} catch(IOException ioe) {
			CM.AlertOffline();
		}
	}
	
	public void login(String username, String password)
	{
		LoginUpdate lu = new LoginUpdate(username, password);
		send(lu);
	}
	
	public void requestInitUpdate()
	{
		//we send an "empty" InitUpdate to let the server know that we want to receive a filled one back
		InitUpdate iu = new InitUpdate(null, null, null, null, CM.getUsername());
		send(iu);
	}
	
	public void register(String username, String password)
	{
		RegisterUpdate ru = new RegisterUpdate(username, password);
		send(ru);
	}
	
	public void sendMessage(ChatMessage m)
	{
		ChatUpdate cu = new ChatUpdate(m);
		send(cu);
	}
	
	public void buyTile(WorldTile wt)
	{
		WorldUpdate wu = new WorldUpdate(wt.getCol(), wt.getRow(), CM.getUsername(), 1);
		send(wu);
	}
	
	public void moveIntoCity(CityTile cityTile)
	{
		CityUpdate cu = new CityUpdate(false, false, cityTile.getCityCol(), cityTile.getCityRow(), cityTile.getWorldCol(),
										cityTile.getWorldRow(), cityTile.getTier(), CM.getUsername());
		send(cu);
	}
	
	public void pickApples(String owner)
	{
		ClickUpdate cu = new ClickUpdate(CM.getUsername(), owner);
		send(cu);
	}
	
	public void buyUpgrade(Upgrade u)
	{
		UpgradeUpdate uu = new UpgradeUpdate(CM.getUsername(), u.getName());
		send(uu);
	}
	
	public void requestCityTileApplesPerClick(CityTile ct)
	{
		RequestPlayerUpdate rpu = new RequestPlayerUpdate(ct.getOwner());
		send(rpu);
	}
	
	public void requestPlayerInformation(String username)
	{
		RequestPlayerInformation rpu = new RequestPlayerInformation(username);
		send(rpu);

	}
	
	private void send(Update u)
	{
		try
		{
			oos.writeObject(u);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run()
	{
		try
		{
			while (true)
			{
				Update update = (Update)ois.readObject();
				processUpdate(update);
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe in run: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("client disconnected");
			System.exit(1);
		}
	}
	
	private void processUpdate(Update update)
	{
		if (update instanceof InitUpdate)
		{
			CM.updateInitialValues((InitUpdate) update);
		}
		else if (update instanceof WorldUpdate)
		{
			CM.updateWorldMap((WorldUpdate) update);
		}
		else if (update instanceof CityUpdate)
		{
			CM.updateCityMap((CityUpdate) update);
		}
		else if (update instanceof ShopUpdate)
		{
			CM.updateShop((ShopUpdate) update);
		}
		else if (update instanceof PlayerUpdate)
		{
			CM.updatePlayer((PlayerUpdate) update);
		}
		else if (update instanceof PlayerInformation)
		{
			CM.updatePlayerInformation((PlayerInformation) update);
		}
		else if (update instanceof LoginUpdate)
		{
			LoginUpdate lu = (LoginUpdate) update;
			CM.notifyLoginSuccess(lu.getUsername(), lu.succeeded());
		}
		else if (update instanceof RegisterUpdate)
		{
			RegisterUpdate ru = (RegisterUpdate) update;
			CM.notifyRegisterSuccess(ru.getUsername(), ru.succeeded());
		}
		else if (update instanceof MoveInUpdate)
		{
			CM.notifyMoveInSuccess((MoveInUpdate) update);
		}
		else if (update instanceof MadeCityUpdate)
		{
			CM.notifyBuyWorldTileSuccess(((MadeCityUpdate) update).created());
		}
		else if (update instanceof RequestPlayerUpdate)
		{
			RequestPlayerUpdate rpu = (RequestPlayerUpdate) update;
			CM.notifyCityTileApplesPerClick(rpu.getPlayerUpdate().getApplesPerClick());
		}
		else if (update instanceof ChatUpdate)
		{
			ChatUpdate cu = (ChatUpdate) update;
			CM.receiveMessage(cu.getChatMessage());
		}
		
		//note: the client should never receive an UpgradeUpdate
	} 
}
