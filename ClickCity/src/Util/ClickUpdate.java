package Util;

public class ClickUpdate extends Update {
	private static final long serialVersionUID = 1L;
	private String username;
	private String propertyOwner;
	
	public ClickUpdate(String username, String propertyOwner){
		this.username = username;
		this.propertyOwner = propertyOwner;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getPropertyOwner(){
		return propertyOwner;
	}
}
