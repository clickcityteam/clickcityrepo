package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import Util.Parser;
import Util.Update;
import Util.pair;

public class ServerManager extends Thread {
	private Server S;
	private Vector<ServerToClientComm> ServerToClientComms;
	private Vector<WorldThread> mWorldThreads;
	public ServerManager(Server S){
		this.S = S;
		this.ServerToClientComms = new Vector<ServerToClientComm>();
		if(InitWorldThreads()){
			this.start();
		}
	}
	private boolean InitWorldThreads() {
		mWorldThreads = new Vector<WorldThread>();
		Vector<pair> worldCoords = S.getMsql().getAllWorldTiles();
		if(worldCoords == null){
			return false;
		}
		for(pair p : worldCoords){
			int x = p.x;
			int y = p.y;
			WorldThread wt = new WorldThread(S, x, y);
			addWorldThread(wt);
		}
		return true;
	}

	@Override
	public void run(){
		int port = new Integer(Parser.getString("port", Parser.serverConfig));
		ServerSocket SS = null;
		try{
			SS = new ServerSocket(port);
			while(true){
				Socket s = SS.accept();
				ServerToClientComm STCC = new ServerToClientComm(s, this, S);
				ServerToClientComms.add(STCC);
			}
			
		}catch(IOException ioe){
			ioe.printStackTrace();
		}finally{
			if(SS != null){
				try{
					SS.close();
				}catch(IOException ioe){
					ioe.printStackTrace();
				}
			}
			
			System.exit(1);
		}
	}

	public void removeSTCC(ServerToClientComm stcc) {
		ServerToClientComms.remove(stcc);
	}
	
	public void PushToAllClients(Update u){
		for(ServerToClientComm stcc : ServerToClientComms){
			stcc.pushObject(u);
		}
	}
	
	public void PushToAClient(Update u, ServerToClientComm stcc){
		stcc.pushObject(u);
	}
	
	public void addWorldThread(WorldThread wt){
		mWorldThreads.add(wt);
	}
}
