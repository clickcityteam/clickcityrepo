package Util;

public class LoginUpdate extends Update{

	private static final long serialVersionUID = 1L;
	private String username;
	private String hashedPassword;
	private boolean succeeded;
	
	public LoginUpdate(String username, String hashedPassword){
		this.username = username;
		this.hashedPassword = hashedPassword;
		this.succeeded = false;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getHashedPassword(){
		return hashedPassword;
	}
	
	public boolean succeeded(){
		return succeeded;
	}
	
	public void authenticate(){
		succeeded = true;
	}

}
