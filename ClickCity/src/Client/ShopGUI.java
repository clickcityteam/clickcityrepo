package Client;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import CustomGUI.CShopButton;
import Util.Audio;
import Util.Upgrade;

public class ShopGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private Vector<CShopButton> clickerButtons;
	private Vector<CShopButton> passiveGeneratorButtons;
	private Vector<CShopButton> cityButtons;
	private JPanel buttonPanel;
	private Shop mShop;
	private int preferedWidth, preferedHeight;
	private JLabel totalAppleLabel, applePerSecondLabel, applePerClickLabel;
	private File soundFile;

	public ShopGUI(Shop shop){
		super("Shop");
		this.mShop = shop;
		instantiateComponents();
		addAction();
		createGUI();
		setLocationRelativeTo(null);
	}
	private void instantiateComponents() {
		preferedWidth = 400;
	    preferedHeight = 100;
	    soundFile = new File("resources/audio/Button_Click_On.wav");
	    totalAppleLabel = new JLabel("Total Apples: ");
	    applePerSecondLabel = new JLabel("Apples per Second: ");
	    applePerClickLabel = new JLabel("Apple per Click: ");
		buttonPanel = new JPanel();
		clickerButtons = new Vector<CShopButton>();
		passiveGeneratorButtons = new Vector<CShopButton>();
		cityButtons = new Vector<CShopButton>();	
	    populateButtons();
	    
	    JScrollPane scrollPane = new JScrollPane(buttonPanel,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
	    JPanel labelPanel = new JPanel();
	    labelPanel.add(totalAppleLabel);
	    labelPanel.add(applePerSecondLabel);
	    labelPanel.add(applePerClickLabel);
	    add(labelPanel, BorderLayout.NORTH);
	    add(scrollPane, BorderLayout.CENTER);
	}
	
	public void populateButtons(){
		buttonPanel.removeAll();
		cityButtons.clear();	
		clickerButtons.clear();
		passiveGeneratorButtons.clear();		
		buttonPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
	    c.fill = GridBagConstraints.BOTH; // components grow in both dimensions
	    c.insets = new Insets(1, 1, 1, 1); // 5-pixel margins on all sides
	 	for (int i=0; i < mShop.getClickerUpgrades().size(); i++){
	    	Upgrade upgrade = mShop.getClickerUpgrades().elementAt(i);
	    	clickerButtons.add(new CShopButton(upgrade.getName() + " (" + Float.toString(upgrade.getEffect()) + ")" + " - cost: " + Integer.toString((int)(upgrade.getBaseCost()*Math.pow((double)upgrade.getMultiplicativeCost(), upgrade.getAmount()))), 
	    			preferedWidth, preferedHeight, upgrade));
	    	if (upgrade.getAmount() == 0 && i < mShop.getClickerUpgrades().size()-1){
	    		Upgrade upgradeNext = mShop.getClickerUpgrades().elementAt(i+1);
	    		CShopButton nextButton = new CShopButton(upgradeNext.getName() + " (" + Float.toString(upgradeNext.getEffect()) + ")" + " - cost: " + Integer.toString((int)(upgradeNext.getBaseCost()*Math.pow((double)upgradeNext.getMultiplicativeCost(), upgradeNext.getAmount()))), 
		    			preferedWidth, preferedHeight, upgradeNext);
	    		nextButton.disableButton();
	    		clickerButtons.add(nextButton);
	    		break;
	    	}
	    }
	    
	    for (int i=0; i < mShop.getPassiveGeneratorUpgrades().size(); i++){
	    	Upgrade upgrade = mShop.getPassiveGeneratorUpgrades().elementAt(i);
	    	passiveGeneratorButtons.add(new CShopButton(upgrade.getName() + " (" + Float.toString(upgrade.getEffect()) + ")" + " - cost: " + Integer.toString((int)(upgrade.getBaseCost()*Math.pow((double)upgrade.getMultiplicativeCost(), upgrade.getAmount()))), 
	    			preferedWidth, preferedHeight, upgrade));
	    	if (upgrade.getAmount() == 0 && i < mShop.getPassiveGeneratorUpgrades().size()-1){
	    		Upgrade upgradeNext = mShop.getPassiveGeneratorUpgrades().elementAt(i+1);
	    		CShopButton nextButton = new CShopButton(upgradeNext.getName() + " (" + Float.toString(upgradeNext.getEffect()) + ")" + " - cost: " + Integer.toString((int)(upgradeNext.getBaseCost()*Math.pow((double)upgradeNext.getMultiplicativeCost(), upgradeNext.getAmount()))), 
		    			preferedWidth, preferedHeight, upgradeNext);
	    		nextButton.disableButton();
	    		passiveGeneratorButtons.add(nextButton);
	    		break;
	    	}
	    }
	    
	    for (int i=0; i < mShop.getCityUpgrades().size(); i++){
	    	Upgrade upgrade = mShop.getCityUpgrades().elementAt(i);
	    	cityButtons.add(new CShopButton(upgrade.getName() + " (" + Float.toString(upgrade.getEffect()) + ")" + " - cost: " + Integer.toString((int)(upgrade.getBaseCost()*Math.pow((double)upgrade.getMultiplicativeCost(), upgrade.getAmount()))), 
	    			preferedWidth, preferedHeight, upgrade));
	    	if (upgrade.getAmount() == 0 && i < mShop.getCityUpgrades().size()-1){
	    		Upgrade upgradeNext = mShop.getCityUpgrades().elementAt(i+1);
	    		CShopButton nextButton = new CShopButton(upgradeNext.getName() + " (" + Float.toString(upgradeNext.getEffect()) + ")" + " - cost: " + Integer.toString((int)(upgradeNext.getBaseCost()*Math.pow((double)upgradeNext.getMultiplicativeCost(), upgradeNext.getAmount()))), 
		    			preferedWidth, preferedHeight, upgradeNext);
	    		nextButton.disableButton();
	    		cityButtons.add(nextButton);
	    		break;
	    	}
	    }
    
	    //ADD BUTTONS TO THE GRIDBAGLAYOUT
	    c.gridx = 0;
	    c.gridy = 0;
	    c.gridwidth = preferedWidth;
	    c.gridheight = 20;
        c.weightx = c.weighty = 0.5;
	    buttonPanel.add(new JLabel("PURCHASE APPLES/CLICK"), c);
	    for (int i=0; i < clickerButtons.size(); i ++){
	    	c.gridx = 0;
	        c.gridy = i*preferedHeight + 20;
	        c.gridwidth = preferedWidth;
	        c.gridheight = preferedHeight;
	        c.weightx = c.weighty = 1.0;
	        buttonPanel.add(clickerButtons.elementAt(i), c);
	    }
	    
	    c.gridx = preferedWidth;
	    c.gridy = 0;
	    c.gridwidth = preferedWidth;
	    c.gridheight = 50;
        c.weightx = c.weighty = 0.5;
	    buttonPanel.add(new JLabel("PURCHASE APPLES/SEC"), c);
	    for (int i=0; i < passiveGeneratorButtons.size(); i ++){
	    	c.gridx = preferedWidth;
	        c.gridy = i*preferedHeight + 20;
	        c.gridwidth = preferedWidth;
	        c.gridheight = preferedHeight;
	        c.weightx = c.weighty = 1.0;
	        buttonPanel.add(passiveGeneratorButtons.elementAt(i), c);
	    }
	    
	    c.gridx = 2*preferedWidth;
	    c.gridy = 0;
	    c.gridwidth = preferedWidth;
	    c.gridheight = 50;
        c.weightx = c.weighty = 0.5;
	    buttonPanel.add(new JLabel("PURCHASE CITY UPGRADE"), c);
	    for (int i=0; i < cityButtons.size(); i ++){
	    	c.gridx = 2*preferedWidth;
	        c.gridy = i*preferedHeight + 20;
	        c.gridwidth = preferedWidth;
	        c.gridheight = preferedHeight;
	        c.weightx = c.weighty = 1.0;
	        buttonPanel.add(cityButtons.elementAt(i), c);
	    }
	    buttonPanel.revalidate();
	    buttonPanel.repaint();
	    addAction();
	}

	private void addAction() {
		// TODO Auto-generated method stub
		//handButton.addActionListener(new UpgradeAdapter(handButton));
		for (int i=0; i < clickerButtons.size(); i ++){
			clickerButtons.elementAt(i).addActionListener(new UpgradeAdapter(clickerButtons.elementAt(i)));
		}
		
		for (int i=0; i < passiveGeneratorButtons.size(); i ++){
			passiveGeneratorButtons.elementAt(i).addActionListener(new UpgradeAdapter(passiveGeneratorButtons.elementAt(i)));
		}
		
		for (int i=0; i < cityButtons.size(); i ++){
			cityButtons.elementAt(i).addActionListener(new UpgradeAdapter(cityButtons.elementAt(i)));
		}
	}

	private void createGUI(){
		setSize(1200, 600);
		setVisible(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public void upgradeResult(Upgrade upgrade, boolean success){
		if (success){
			if (upgrade.getType() == Upgrade.UpgradeType.clicker){
				for (int i=0; i< clickerButtons.size(); i++){
					if (clickerButtons.elementAt(i).getUpgradeName().equals(upgrade.getName())){
						clickerButtons.elementAt(i).upgrade();
					}
				}
			} else if (upgrade.getType() == Upgrade.UpgradeType.passiveGenerator){
				for (int i=0; i< passiveGeneratorButtons.size(); i++){
					if (passiveGeneratorButtons.elementAt(i).getUpgradeName().equals(upgrade.getName())){
						passiveGeneratorButtons.elementAt(i).upgrade();
					}
				}
			} else if (upgrade.getType() == Upgrade.UpgradeType.city){
				for (int i=0; i< cityButtons.size(); i++){
					if (cityButtons.elementAt(i).getUpgradeName().equals(upgrade.getName())){
						cityButtons.elementAt(i).upgrade();
					}
				}
			} 
		} else {
			JOptionPane.showInternalMessageDialog(this, "failed to upgrade!",
			        "upgrade error", JOptionPane.ERROR_MESSAGE);
		}
		addAction();
	}
	
	public void displayPlayerInfo(double apples, double applesPerSecond, double applesPerClick){
		totalAppleLabel.setText("Total Apples: " + Double.toString(apples));
		applePerSecondLabel.setText("Apples per second: " + Double.toString(applesPerSecond));	
		applePerClickLabel.setText("Apples per Click: " + Double.toString(applesPerClick));
	}
	
	class UpgradeAdapter implements ActionListener {
		private CShopButton button;
		public UpgradeAdapter(CShopButton button){
			this.button = button;
		}
	
		@Override
		public void actionPerformed(ActionEvent e) {
			Audio.PlaySound(soundFile);
			Upgrade upgrade = button.getUpgrade();
			mShop.getClientManager().buyUpgrade(upgrade);
		}
	}
}
