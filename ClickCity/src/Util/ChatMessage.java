package Util;

import java.io.Serializable;

public class ChatMessage implements Serializable{
	public static final long serialVersionUID=1;
	
	private String sender;
	private String message;
	private boolean isPrivate;
	private String recipient;
	
	public ChatMessage(String sender, String message){
		this.sender=sender;
		this.message=message;
		this.isPrivate=false;
		this.recipient=null;
	}
	
	public ChatMessage(String sender, String message, String recipient)
	{
		this.sender=sender;
		this.message=message;
		this.isPrivate=true;
		this.recipient=recipient;
	}
	
	public String getRecipient()
	{
		return recipient;
	}
	
	public boolean isPrivate()
	{
		return isPrivate;
	}
	
	public String getSender() 
	{
		return sender;
	}
	public String getMessage() 
	{
		return message;
	}
	
}
