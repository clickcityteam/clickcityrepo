package CustomGUI;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import Client.WorldMapGUI;
import Client.WorldTile;
import Util.Audio;
import Util.WorldUpdate;

public class CWorldTile extends CButton implements ActionListener{
	private static final long serialVersionUID = 1L;
	private String mCurrImageFolder;
	protected ImageIcon mDefaultIcon;
	protected ImageIcon mHoverIcon;
	protected ImageIcon mPressedIcon;
	protected ImageIcon mDisabledIcon;
	private WorldTile mWorldTile;
	private WorldMapGUI mWorldMapGUI;

	/*
	 * constructor
	 * @param GridPosition: gridPosition
	 * @param int: width
	 * @param int: height
	 * @param WorldMapGUI: worldMapGUI
	 * @param ClientManager: clientManager
	 */
	public CWorldTile(WorldTile worldTile, WorldMapGUI worldMapGUI)
	{
		super("");
		mWorldMapGUI = worldMapGUI;
		this.hasRaisedBevelBorder(false);
		this.setPreferredSize(new Dimension(worldTile.GetWidth(),worldTile.GetHeight()));
		
		//Setting images for the city tile
		this.addActionListener(this);
		mCurrImageFolder="Grass";
		Initialize();
		mWorldTile=worldTile;
		this.setToolTipText("City owned by "+mWorldTile.getOwnerName());
	}
	
	private void Initialize()
	{
   	 	super.setDefaultIcon(new ImageIcon("resources/images/"+mCurrImageFolder+"/base.png"));
	   	super.setHoverIcon(new ImageIcon("resources/images/"+mCurrImageFolder+"/highlighted.png"));
	   	//super.setDisabledIcon(new ImageIcon("resources/defaultbuttondisabled.jpg"));
	   	super.setPressedIcon(new ImageIcon("resources/images/"+mCurrImageFolder+"/clicked.png"));
	}
	
	public void updateTile(WorldUpdate worldUpdate)
	{
		int tier=worldUpdate.getTier();
		String owner= worldUpdate.getOwner();
		setCityType(tier);
		setOwner(owner);
	}
	
	private void setOwner(String owner)
	{
		mWorldTile.setOwner(owner);
		this.setToolTipText("City owned by "+mWorldTile.getOwnerName());
	}
	
	private void setCityType(int tier)
	{
		mWorldTile.setTier(tier);
		mCurrImageFolder="City_"+tier;
	   	super.setDefaultIcon(new ImageIcon("resources/images/"+mCurrImageFolder+"/base.png"));
	   	super.setHoverIcon(new ImageIcon("resources/images/"+mCurrImageFolder+"/highlighted.png"));
		//super.setDisabledIcon(new ImageIcon("resources/defaultbuttondisabled.jpg"));
		super.setPressedIcon(new ImageIcon("resources/images/"+mCurrImageFolder+"/clicked.png"));
		super.revalidate();
		super.repaint();
	}

	/*
	 * show that you want to buy city
	 */
	public void showConfirmBuyDialog()
	{
		int result=JOptionPane.showConfirmDialog(this,
					"Confirm you want try to purchase this city. It will cost 100,000 apples.",
					"Confirmation",
					JOptionPane.YES_NO_OPTION);
		if(result==0)
		{
			Audio.PlaySound(new File("resources/audio/Button_Click_On.wav"));
			mWorldTile.getClientManager().buyTile(mWorldTile);
		}
		else if(result==1)
		{
			Audio.PlaySound(new File("resources/audio/Button_Click_Off.wav"));
		}
	}

	/*
	 * show that you want to go to city view
	 */
	private void showConfirmCityDialog()
	{
		int result=JOptionPane.showConfirmDialog(this,
					"Do you want to enter this city?",
					"Confirmation",
					JOptionPane.YES_NO_OPTION);
		if(result==0)
		{
			Audio.PlaySound(new File("resources/audio/Button_Click_On.wav"));
			mWorldTile.getClientManager().openCity(mWorldTile);
			mWorldMapGUI.setVisible(false);
		}
		else if(result==1)
		{
			Audio.PlaySound(new File("resources/audio/Button_Click_Off.wav"));
		}
	}
	
	/*
	 * implement actionPerformed to register button clicks
	 * @param ActionEvent: e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//if this button is pressed
		if(e.getSource()==this)
		{
			//Play click sound
			Audio.PlaySound(new File("resources/audio/Button_Click_On.wav"));
			if(mWorldTile.isOwned())
			{
				showConfirmCityDialog();
			}
			else
			{
				showConfirmBuyDialog();
			}
		}
	}
}
