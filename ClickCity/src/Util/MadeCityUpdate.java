package Util;

public class MadeCityUpdate extends Update {

	private static final long serialVersionUID = 1L;
	
	private boolean created;
	private String owner;
	
	public MadeCityUpdate(boolean created, String owner){
		this.created = created;
		this.owner = owner;
	}
	
	public boolean created(){
		return created;
	}
	
	public String getOwner(){
		return owner;
	}
}
