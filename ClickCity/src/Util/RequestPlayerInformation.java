package Util;

public class RequestPlayerInformation extends Update {
	private static final long serialVersionUID = 1L;
	private String username;
	private PlayerInformation pi;
	public RequestPlayerInformation(String username){
		this.username = username;
	}
	
	public void fillPlayerUpdate(PlayerInformation pi){
		this.pi = pi;
	}
	
	public String getUsername(){
		return username;
	}
	
	public PlayerInformation getPlayerUpdate(){
		return pi;
	}
}
