package Client;

import java.util.Vector;

import Util.ShopUpdate;
import Util.Upgrade;

public class Shop {
	private ShopGUI shopGUI;
	private Vector<Upgrade> vClickerUpgrades;
	private Vector<Upgrade> vPassiveGeneratorUpgrades;
	private Vector<Upgrade> vCityUpgrades;
	private ClientManager cm;
	
	public Shop(ClientManager cm){ 
		this.cm = cm;
		shopGUI = new ShopGUI(this); 
		shopGUI.setVisible(false);
	}
	
	{
		vClickerUpgrades = new Vector<Upgrade>();
		vPassiveGeneratorUpgrades = new Vector<Upgrade>();
		vCityUpgrades = new Vector<Upgrade>();
	}
	
	public void openShop(){
		shopGUI.setVisible(true);
	}
	
	public void closeShop(){
		shopGUI.setVisible(false);
	}
	
	public Vector<Upgrade> getClickerUpgrades() {
		return vClickerUpgrades;
	}
	
	public Vector<Upgrade> getPassiveGeneratorUpgrades(){
		return vPassiveGeneratorUpgrades;
	}
	
	public Vector<Upgrade> getCityUpgrades(){
		return vCityUpgrades;
	}
	
	public void notifyUpgradePurchase(Upgrade upgrade, boolean success){
		shopGUI.upgradeResult(upgrade, success);
	}
	
	public void Update(ShopUpdate shopUpdate){
		vClickerUpgrades.clear();
		vPassiveGeneratorUpgrades.clear();
		vCityUpgrades.clear();
		Vector<Upgrade> allUpgrade = shopUpdate.getUpgrades();
		for (int i=0; i<allUpgrade.size(); i++){
			Upgrade upgrade = allUpgrade.elementAt(i);
			if (upgrade.getType() == Upgrade.UpgradeType.clicker){
				vClickerUpgrades.add(upgrade);
			} else if (upgrade.getType() == Upgrade.UpgradeType.passiveGenerator){
				vPassiveGeneratorUpgrades.add(upgrade);
			} else if (upgrade.getType() == Upgrade.UpgradeType.city){
				vCityUpgrades.add(upgrade);
			}
		}
		shopGUI.populateButtons();
	}
	
	public ClientManager getClientManager(){
		return cm;
	}
	
	public void notifyPlayerChange(){
		shopGUI.displayPlayerInfo(cm.getApples(), cm.getApplesPerSecond(), cm.getApplesPerClick());
	}

	
}
