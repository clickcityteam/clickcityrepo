package Server;

import Util.CityUpdate;
import Util.PlayerUpdate;

public class AppleThread extends Thread {
	private Server s;
	private ServerToClientComm stcc;
	private String username;
	private boolean connected;
	public AppleThread(Server S, ServerToClientComm stcc, String username){
		this.s = S;
		this.stcc = stcc;
		this.username = username;
		connected = true;
		this.start();
	}
	
	public void run(){
		try{
			while(connected){
				synchronized(s.getMsql()){
					if(s.getMsql().UpdatePlayer(username)){
						PlayerUpdate pu = s.getMsql().getPlayerUpdate(username);
						stcc.pushObject(pu);
						CityUpdate cu = s.getMsql().updatePropertyTier(username);
						stcc.pushObject(cu);
					}
					else{ 
						break;
					}
				}
				Thread.sleep(1000);
			}
		} catch (InterruptedException ie){
			disconnect();
		}
		
	}
	public void disconnect(){
		connected = false;
	}
}
